<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'social' => [
        'facebook' => [
            'client_id' => '757496755012532',
            'client_secret' => '08462d7f9b46e3e39a421139ac4cd27f',
        ],
        'google' => [
            'client_id' => '655972310277-97ipq9mdp7h1a2fv97tl0sdatqkvecej.apps.googleusercontent.com',
            'client_secret' => 'KHS1Go7DlIpmJpuK-rapnZ4E',
        ],
        'yandex' => [
            'client_id' => '9450711b57364fa0b1dc425462d5f562',
            'client_secret' => '1fcb5c70563c4ead87c0541c3b00abe4',
        ],
        'mailru' => [
            'client_id' => '336b2108d2b34abbb59ef02a1c54345e',
            'client_secret' => 'db9fdffb06c74e7481a6e9c380c5e372',
        ],
        'vkontakte' => [
            'client_id' => '7612578',
            'client_secret' => 'YST3m65ok1oxPvkOMz8L',
        ],
    ]

];
