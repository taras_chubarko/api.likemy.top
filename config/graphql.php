<?php

declare(strict_types=1);

use example\Mutation\ExampleMutation;
use example\Query\ExampleQuery;
use example\Type\ExampleRelationType;
use example\Type\ExampleType;

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each route
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    // ]
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Rebing\GraphQL\GraphQLController::class.'@query',

    // Any middleware for the graphql route group
    'middleware' => [],

    // Additional route group attributes
    //
    // Example:
    //
    // 'route_group_attributes' => ['guard' => 'api']
    //
    'route_group_attributes' => [],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'default_schema' => 'default',

    // The schemas for query and/or mutation. It expects an array of schemas to provide
    // both the 'query' fields and the 'mutation' fields.
    //
    // You can also provide a middleware that will only apply to the given schema
    //
    // Example:
    //
    //  'schema' => 'default',
    //
    //  'schemas' => [
    //      'default' => [
    //          'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //      ],
    //      'user' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\ProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ],
    //      'user/me' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\MyProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ],
    //  ]
    //
    'schemas' => [
        'default' => [
            'query' => [
                // 'example_query' => ExampleQuery::class,
                \App\GraphQL\Queries\MovieListQuery::class,
                \App\GraphQL\Queries\MoviesPaginateQuery::class,
                \App\GraphQL\Queries\MoviesFilterQuery::class,
                \App\GraphQL\Queries\MovieQuery::class,
                \App\GraphQL\Queries\MovieSimilarQuery::class,
                \App\GraphQL\Queries\MovieFromDirectorQuery::class,
                \App\GraphQL\Queries\MovieActorsQuery::class,
                \App\GraphQL\Queries\MovieCommentsQuery::class,
                \App\GraphQL\Queries\UserCollectionItemsQuery::class,
                \App\GraphQL\Queries\CollectionsInMainQuery::class,
                \App\GraphQL\Queries\CollectionsQuery::class,
                \App\GraphQL\Queries\CollectionsByUserQuery::class,
                \App\GraphQL\Queries\MovieSearchQuery::class,
                \App\GraphQL\Queries\MovieTopDayQuery::class,
                \App\GraphQL\Queries\MovieRandomQuery::class,
                \App\GraphQL\Queries\MovieSequelsAndPrequelsQuery::class,
                \App\GraphQL\Queries\MovieActorQuery::class,
                \App\GraphQL\Queries\MovieActorFilmsQuery::class,
                \App\GraphQL\Queries\MovieInMainQuery::class,
                \App\GraphQL\Queries\MoviesFilmPlusFilmSearch::class,
                \App\GraphQL\Queries\MoviesFilmPlusFilm::class,
                \App\GraphQL\Queries\MoviesFilmPlusFilmRandom::class,
            ],
            'mutation' => [
                // 'example_mutation'  => ExampleMutation::class,
                \App\GraphQL\Mutations\UserLoginMutation::class,
                \App\GraphQL\Mutations\UserRegisterMutation::class,
                \App\GraphQL\Mutations\UserSocAuthMutation::class,
                \App\GraphQL\Mutations\UserRegisterCheckMutation::class,
                \App\GraphQL\Mutations\UserEmailActivation::class,
                \App\GraphQL\Mutations\UserSocDataMutation::class,
            ],
            'middleware' => [\App\Http\Middleware\UserActive::class],
            'method' => ['get', 'post'],
        ],
        'secret' => [
            'query' => [
                // 'example_query' => ExampleQuery::class,
                \App\GraphQL\Queries\UserProfileQuery::class,
                \App\GraphQL\Queries\UserProfileTabsQuery::class,
                \App\GraphQL\Queries\UserFollowersQuery::class,
                \App\GraphQL\Queries\UserFollowingsQuery::class,
                \App\GraphQL\Queries\MovieMyLikeQuery::class,
                \App\GraphQL\Queries\CollectionsInSettQuery::class,
                \App\GraphQL\Queries\UserBookMarkMenuQuery::class,
                \App\GraphQL\Queries\UserBookMarkQuery::class,
                \App\GraphQL\Queries\UserCollectionsQuery::class,
                \App\GraphQL\Queries\UserPreferencesQuery::class,
                \App\GraphQL\Queries\UserLikesCountQuery::class,
                \App\GraphQL\Queries\UserLikesMovieQuery::class,
                \App\GraphQL\Queries\UserCommentsMovieQuery::class,
                \App\GraphQL\Queries\UserFeedTabsQuery::class,
                \App\GraphQL\Queries\UserChosenQuery::class,
                \App\GraphQL\Queries\UserDegreeQuery::class,
                \App\GraphQL\Queries\UserFeedTimeLineQuery::class,
               \App\GraphQL\Queries\MessagesInboxQuery::class,
                \App\GraphQL\Queries\MessageReadQuery::class,
                \App\GraphQL\Queries\MessagesSentQuery::class,
                \App\GraphQL\Queries\MessagesStarredQuery::class,
                \App\GraphQL\Queries\MessagesTrashedQuery::class,
                \App\GraphQL\Queries\MessagesUnreadQuery::class,
                \App\GraphQL\Queries\MessagesChatsQuery::class,
                \App\GraphQL\Queries\ChatMessgesQuery::class,

            ],
            'mutation' => [
                // 'example_mutation'  => ExampleMutation::class,
                \App\GraphQL\Mutations\UserLogoutMutation::class,
                \App\GraphQL\Mutations\MovieAddLikeMutation::class,
                \App\GraphQL\Mutations\MovieChangeLikeMutation::class,
                \App\GraphQL\Mutations\MovieCommentAddMutation::class,
                \App\GraphQL\Mutations\MovieCommentReviewAddMutation::class,
                \App\GraphQL\Mutations\MovieCommentAddLikeMutation::class,
                \App\GraphQL\Mutations\BookMarkAddMutation::class,
                \App\GraphQL\Mutations\CollectionAddMutation::class,
                \App\GraphQL\Mutations\CollectionItemAddMutation::class,
                \App\GraphQL\Mutations\UserFollowRequestMutation::class,
                \App\GraphQL\Mutations\UserChosenMutation::class,
                \App\GraphQL\Mutations\MessageSendMutation::class,
                \App\GraphQL\Mutations\MessageAddStarredMutation::class,
                \App\GraphQL\Mutations\MessageAddTrashMutation::class,
                \App\GraphQL\Mutations\ChatMessageAddMutation::class,
                \App\GraphQL\Mutations\CollectionRemoveMutation::class,
            ],
            'middleware' => ['auth:api', \App\Http\Middleware\UserActive::class],
            'method' => ['get', 'post'],
        ],
    ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [
        // 'example'           => ExampleType::class,
        // 'relation_example'  => ExampleRelationType::class,
        // \Rebing\GraphQL\Support\UploadType::class,
        \App\GraphQL\Types\TokenType::class,
        \App\GraphQL\Types\UserType::class,
        \App\GraphQL\Types\FileType::class,
        \App\GraphQL\Types\TokenDataType::class,
        \App\GraphQL\Types\UserProfileTabsType::class,
        \App\GraphQL\Types\UserFollowersType::class,
        \App\GraphQL\Types\MovieType::class,
        \App\GraphQL\Types\MovieInMainType::class,
        \App\GraphQL\Types\MovieTagsType::class,
        \App\GraphQL\Inputs\MoviesPaginateInput::class,
        \App\GraphQL\Inputs\FromToInput::class,
        \App\GraphQL\Inputs\MinMaxIntInput::class,
        \App\GraphQL\Types\MoviesFilterType::class,
        \App\GraphQL\Types\MoviesFilterObjectType::class,
        \App\GraphQL\Types\MovieVideoURLType::class,
        \App\GraphQL\Types\MoviePeopleType::class,
        \App\GraphQL\Types\MovieRatingsType::class,
        \App\GraphQL\Types\MovieCommentType::class,
        \App\GraphQL\Types\MovieLikeType::class,
        \App\GraphQL\Types\MovieCommentLikeType::class,
        \App\GraphQL\Types\MovieCommentStatisticType::class,
        \App\GraphQL\Types\BookMarkType::class,
        \App\GraphQL\Types\CollectionType::class,
        \App\GraphQL\Types\UserBookMarkMenuType::class,
        \App\GraphQL\Types\UserCollectionItemsType::class,
        \App\GraphQL\Types\UsePreferencesType::class,
        \App\GraphQL\Types\UsePreferencesDataType::class,
        \App\GraphQL\Types\UsePreferencesYearsType::class,
        \App\GraphQL\Types\UseLikesCountType::class,
        \App\GraphQL\Inputs\UseLikesMovieItput::class,
        \App\GraphQL\Inputs\UseCommentsMovieInput::class,
        \App\GraphQL\Types\MessageType::class,
        \App\GraphQL\Inputs\MessageSendInput::class,
        \App\GraphQL\Types\ChatMessgeType::class,
        \App\GraphQL\Types\ChatType::class,
        \App\GraphQL\Types\MovieSearchType::class,
        \App\GraphQL\Types\GenreStatisticType::class,
        \App\GraphQL\Types\TimeLineType::class,
        \App\GraphQL\Types\TimeLineItemsType::class,
        \App\GraphQL\Types\TimeLineItemsItemType::class,
        \App\GraphQL\Types\UserSocDataType::class,
        \App\GraphQL\Types\MinMaxType::class,
        \App\GraphQL\Inputs\UserDegreeInput::class,
        \App\GraphQL\Types\MovieBudgetType::class,
        \App\GraphQL\Types\MoviePremierType::class,
    ],

    // The types will be loaded on demand. Default is to load all types on each request
    // Can increase performance on schemes with many types
    // Presupposes the config type key to match the type class name property
    'lazyload_types' => false,

    // This callable will be passed the Error object for each errors GraphQL catch.
    // The method should return an array representing the error.
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError'],

    /*
     * Custom Error Handling
     *
     * Expected handler signature is: function (array $errors, callable $formatter): array
     *
     * The default handler will pass exceptions to laravel Error Handling mechanism
     */
    'errors_handler' => ['\Rebing\GraphQL\GraphQL', 'handleErrors'],

    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key' => 'variables',

    /*
     * Options to limit the query complexity and depth. See the doc
     * @ https://webonyx.github.io/graphql-php/security
     * for details. Disabled by default.
     */
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null,
        'disable_introspection' => false,
    ],

    /*
     * You can define your own pagination type.
     * Reference \Rebing\GraphQL\Support\PaginationType::class
     */
    'pagination_type' => \Rebing\GraphQL\Support\PaginationType::class,

    /*
     * Config for GraphiQL (see (https://github.com/graphql/graphiql).
     */
    'graphiql' => [
        'prefix' => '/graphiql',
        'controller' => \Rebing\GraphQL\GraphQLController::class.'@graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql',
        'display' => env('ENABLE_GRAPHIQL', true),
    ],

    /*
     * Overrides the default field resolver
     * See http://webonyx.github.io/graphql-php/data-fetching/#default-field-resolver
     *
     * Example:
     *
     * ```php
     * 'defaultFieldResolver' => function ($root, $args, $context, $info) {
     * },
     * ```
     * or
     * ```php
     * 'defaultFieldResolver' => [SomeKlass::class, 'someMethod'],
     * ```
     */
    'defaultFieldResolver' => null,

    /*
     * Any headers that will be added to the response returned by the default controller
     */
    'headers' => [],

    /*
     * Any JSON encoding options when returning a response from the default controller
     * See http://php.net/manual/function.json-encode.php for the full list of options
     */
    'json_encoding_options' => 0,
];
