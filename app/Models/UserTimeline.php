<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTimeline extends Model
{
    //
    protected $table = 'user_timeline';

    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }

    /* public function like
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function like()
    {
        return $this->belongsTo(MovieLike::class, 'like_id', 'id');
    }

    /* public function comment
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function comment()
    {
        return $this->belongsTo(MovieComment::class, 'comment_id', 'id');
    }

    /* public function getDtAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDtAttribute()
    {
        return $this->updated_at->format('d.m.Y');
    }
}
