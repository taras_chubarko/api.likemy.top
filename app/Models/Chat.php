<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    protected $table = 'chat';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function sender
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    /* public function user
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /* public function getPositionAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPositionAttribute()
    {
        $u = !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null;
        if($u == $this->sender_id){
            return 'right';
        }else{
            return 'left';
        }
    }

    /* public function
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTimeAttribute()
    {
        return $this->created_at->format('H:i');
    }

    /* public function getDtAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDtAttribute()
    {
        return $this->created_at->format('d.m.Y');
    }
}
