<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageStarred extends Model
{
    //
    protected $table = 'message_starred';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
