<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Collection extends Model
{
    use Sluggable;
    //
    protected $table = 'collections';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /* public function items
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function items()
    {
        return $this->hasMany(CollectionItem::class, 'collection_id', 'id')->orderBy('created_at', 'desc');
    }

    /* public function count
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCountItemsAttribute()
    {
        return $this->items()->count();
    }

    /* public function getPosterAttribete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPosterImageAttribute()
    {
        if($this->items()->count()){
            $movie = $this->items[0]->movie;
            if(isset($movie->bigPosterURL)){
                return $movie->bigPosterURL;
            }
        }
        return config('app.url') . '/img/original/video.poster.png';
    }

    /* public function user
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
