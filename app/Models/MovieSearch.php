<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieSearch extends Model
{
    //
    protected $table = 'movie_search';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }
}
