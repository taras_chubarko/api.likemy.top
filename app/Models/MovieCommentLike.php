<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieCommentLike extends Model
{
    //movie_comment_like
    //
    protected $table = 'movie_comment_like';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
