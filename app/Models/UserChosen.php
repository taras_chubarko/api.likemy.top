<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserChosen extends Model
{
    //
    protected $table = 'user_chosen';

    protected $guarded = [
        '_method',
        '_token'
    ];
}
