<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Message extends Model
{
    //
    protected $table = 'messages';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function sender
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function sender()
    {
        $u = !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null;
        if($u == $this->sender_id){
            return $this->belongsTo(User::class, 'user_id', 'id');
        }else{
            return $this->belongsTo(User::class, 'sender_id', 'id');
        }
    }

    /* public function getBodyShortAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBodyShortAttribute()
    {
        return Str::limit($this->attributes['body'], 150);
    }

    /* public function user
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        //return $this->belongsTo(User::class, 'user_id', 'id');
        $u = !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null;
        if($u == $this->sender_id){
            return $this->belongsTo(User::class, 'user_id', 'id');
        }else{

            return $this->belongsTo(User::class, 'sender_id', 'id');
        }
    }

    /* public function starredModel
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function starredModel()
    {
        return $this->hasOne(MessageStarred::class, 'message_id', 'id')
            ->where('user_id', !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null);
    }

    /* public function getStarredAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStarredAttribute()
    {
        return $this->starredModel ? true : false;
    }

    /* public function inTrash
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inTrash()
    {
        return $this->hasOne(MessageAction::class, 'message_id', 'id')
            ->where('action', 'trash')
            ->where('user_id', !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null);
            //->orWhere('sender_id', !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null);
    }

    /* public function chat
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function chat()
    {
        return $this->hasMany(Chat::class, 'chat_id', 'id');
    }
}
