<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageAction extends Model
{
    //
    protected $table = 'message_actions';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
