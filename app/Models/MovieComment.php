<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MovieComment extends Model
{
    //
    protected $table = 'movie_comments';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function getDtAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    /* public function user
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /* public function children
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function children()
    {
        return $this->hasMany(MovieComment::class, 'parent_id', 'id');
    }

    /* public function alllikes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function allLikes()
    {
        return $this->hasMany(MovieCommentLike::class,  'movie_comment_id', 'id');
    }

    /* public function positiveLikes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function positiveLikes()
    {
        return $this->hasMany(MovieCommentLike::class,  'movie_comment_id', 'id')->where('type', 'positive');
    }

    /* public function negativeLikes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function negativeLikes()
    {
        return $this->hasMany(MovieCommentLike::class,  'movie_comment_id', 'id')->where('type', 'negative');
    }

    /* public function likes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getLikesAttribute()
    {
        $arr['positive'] = $this->allLikes->where('type', 'positive')->count();
        $arr['negative'] = $this->allLikes->where('type', 'negative')->count();
        return $arr;
    }

    /* public function movieLikes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function types()
    {
        //dd($this->user_id);
        return MovieLike::where('user_id', $this->user_id)->where('movie_id', $this->movie_id)->first();
    }

    /* public function getTypeAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTypeAttribute()
    {
        return $this->types() ? $this->types()->type : null;
    }

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }


}
