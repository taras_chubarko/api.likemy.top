<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageTrashed extends Model
{
    //
    protected $table = 'message_trashed';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
