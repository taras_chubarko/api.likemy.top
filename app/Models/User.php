<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Hootlex\Friendships\Traits\Friendable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Overtrue\LaravelFollow\Followable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Sluggable, Followable, Friendable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'login', 'email',
        'password', 'gender', 'birthday', 'avatar',
        'bg', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'forSlug'
            ]
        ];
    }

    /* public function setAvatarAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setAvatarAttribute($value)
    {
        $this->attributes['avatar'] = json_encode($value);
    }

    /* public function getAvatarAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAvatarAttribute()
    {
        if (isset($this->attributes['avatar'])) {
            return json_decode($this->attributes['avatar']);
        } else {
            return (object)[];
        }
    }

    /* public function setBgAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setBgAttribute($value)
    {
        $this->attributes['bg'] = json_encode($value);
    }

    /* public function getBgAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBgAttribute()
    {
        if (isset($this->attributes['bg'])) {
            return json_decode($this->attributes['bg']);
        } else {
            return (object)[];
        }
    }

    /* public function getAvatarImage
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAvatarImageAttribute()
    {
        $data = json_decode($this->attributes['avatar']);
        if ($data) {
            return config('app.url') . '/img/original/' . $data->basename;
        }
        return config('app.url') . '/img/original/nouser.png';
    }

    /* public function getBgImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBgImageAttribute()
    {
        $data = json_decode($this->attributes['bg']);
        if ($data) {
            return config('app.url') . '/img/original/' . $data->basename;
        }
        //return config('app.url') . '/img/original/nobg.jpg';
        return null;
    }

    /* public function getBirthdayAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBirthdayAttribute()
    {
        return Carbon::parse($this->attributes['birthday'])->format('d.m.Y');
    }

    /* public function getLlast_activityAttribue
     * @param
     *-----------------------------------
     *|last_activity
     *-----------------------------------
     */
    public function getLastActivityAttribute()
    {
        if ($this->attributes['last_activity']) {
            return Carbon::parse($this->attributes['last_activity'])->format('d.m.Y H:i:s');
        } else {
            return null;
        }

    }

    /* public function getForSlugAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getForSlugAttribute()
    {
        if (isset($this->attributes['login'])) {
            return $this->attributes['login'];
        } else {
            if (isset($this->attributes['email'])) {
                $exp = explode('@', $this->attributes['email']);
                return $exp[0];
            } else {
                return 'likemytopuser';
            }
        }
    }

    /* public function socialProfiles
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function socialProfiles()
    {
        return $this->hasMany(UserSocial::class, 'user_id', 'id');
    }

    /* public function collections
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function collections()
    {
        return $this->hasMany(Collection::class, 'user_id', 'id')->orderBy('created_at', 'desc');
    }

    /* public function bookmarks
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function bookmarks()
    {
        return $this->hasMany(BookMark::class, 'user_id', 'id');
    }

    /* public function viewedMovies
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function viewedMovies()
    {
        return $this->hasMany(BookMark::class, 'user_id', 'id')->where('type', 'viewed')->orderBy('created_at', 'desc');
    }

    /* public function likes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function likes()
    {
        return $this->hasMany(MovieLike::class, 'user_id', 'id')->orderBy('created_at', 'desc');
    }

    /* public function movieComments
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movieComments()
    {
        return $this->hasMany(MovieComment::class, 'user_id', 'id')->whereNull('parent_id')->orderBy('created_at', 'desc');
    }

    /* public function getIfollowAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getIfollowAttribute()
    {
        $user = User::find($_SERVER['HTTP_U']);
        return $user->isFollowing($this->id);
    }

    /* public function chosens
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function chosens()
    {
        return $this->hasMany(UserChosen::class, 'user_id', 'id')->orderBy('created_at', 'desc');
    }

    /* public function degree
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function degree()
    {
        $count = 0;
        $likes = $this->likes;
        $me = User::find($_SERVER['HTTP_U']);
        $meLikes = $me->likes;
        //
        $arr = [];
        $arr2 = [];

        if($meLikes && $likes){
            foreach ($meLikes as $like){
                $m = $likes->where('movie_id', $like->movie_id)->first();
                $m2 = $likes->where('movie_id', $like->movie_id)->where('type', $like->type)->first();
                if($m){
                    $arr[] = $m;
                }
                if($m2){
                    $arr2[] = $m2;
                }
            }
        }
        //dd($arr);
        $arr = collect($arr);//общих фильмов у нас
        $arr2 = collect($arr2);//одинаково мы оценили



        $count = $arr2->count() > 0 ? (int) (($arr2->count() / $arr->count()) * 100) : 0;

        $data['common'] = $arr->count();
        $data['equally'] = $arr2->count();
        $data['count'] = $count;

        return $data;
    }

    /* public function getPercentDegreeAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPercentDegreeAttribute()
    {
        return $this->degree()['count'];
    }

    /* public function timeline
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function timeline()
    {
        return $this->hasManyThrough(MovieComment::class, MovieLike::class, 'user_id', 'user_id', 'id');
    }

    /* public function inbox
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox()
    {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }

    /* public function sent
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function sent()
    {
        return $this->hasMany(Message::class, 'sender_id', 'id');
    }

    /* public function getUnreadMessagesAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getUnreadMessagesAttribute()
    {
        return $this->inbox()->where('read', 0)->count();
    }

    /* public function myLikesInRecomended
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function myLikesInRecommend($type)
    {
        $q = $this->likes();
        //$q->with('movie');
        $q->whereHas('movie', function ($query) use ($type) {
            if ($type == 'films') {
                $query->where('genre', 'NOT LIKE', '%мультфильм%');
                $query->where('genre', 'NOT LIKE', '%аниме%');
                $query->where('type', 'KPFilm');
            }
            if ($type == 'serials') {
                $query->where('genre', 'NOT LIKE', '%мультфильм%');
                $query->where('genre', 'NOT LIKE', '%аниме%');
                $query->where('type', 'KPSerial');
            }
            if ($type == 'cartoons') {
                $query->where('genre', 'LIKE', '%мультфильм%');
            }

        });
        $count = $q->count();

        $count = 10 - $count;

        return $count >= 1 ? $count : null;
    }

    /* public function getInFollowersAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getInFollowersAttribute()
    {
        $me = User::find($_SERVER['HTTP_U']);

        //dd($this);
        $meFollowers = $me->followings->where('id', $this->id)->first();
        return $meFollowers ? true : false;
    }

    /* public function getCoincidenceAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCoincidenceAttribute()
    {
        return $this->degree()['equally'].' из '.$this->degree()['common'];
    }

}
