<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieView extends Model
{
    protected $table = 'movie_view';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
