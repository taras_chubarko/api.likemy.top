<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieFilter extends Model
{
    protected $table = 'movies_filters';


    protected $casts = [
        'genre' => 'array',
        'country' => 'array',
        'year' => 'array',
    ];
    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
