<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookMark extends Model
{
    //
    protected $table = 'bookmark';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }
}
