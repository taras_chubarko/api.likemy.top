<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KinopoiskRating extends Model
{
    //kinopoisk_rating
    protected $table = 'kinopoisk_rating';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
