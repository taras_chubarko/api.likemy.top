<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieLike extends Model
{
    protected $table = 'movie_likes';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }

    /* public function user
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
