<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parser extends Model
{
    //
    //
    protected $table = 'parser';

    protected $guarded = [
        '_method',
        '_token'
    ];
}
