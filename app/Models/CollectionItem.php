<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionItem extends Model
{
    protected $table = 'collections_items';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function movie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }
}
