<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    //
    protected $table = 'users_social';

    public $timestamps = false;

    protected $casts = [
        'data' => 'array'
    ];

    protected $guarded = [
        '_method',
        '_token'
    ];

}
