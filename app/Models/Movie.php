<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use Sluggable;

    protected $guarded = [
        '_method',
        '_token'
    ];

    protected $casts = [
        'ratingData' => 'array',
        'posterSize' => 'array',
        'videoURL' => 'array',
        'rentData' => 'array',
        'budgetData' => 'array',
        'gallery' => 'array',
        'creators' => 'array',
        'topNewsByFilm' => 'array',
        'triviaData' => 'array',
        'seriesInfo' => 'array',
    ];

    //
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nameRU'
            ]
        ];
    }

    /* public function ratings
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function ratings()
    {
        return $this->hasOne(MovieRating::class, 'movie_id', 'id');
    }

    /* public function kinopoiskRatings
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function kinopoiskRatings()
    {
        return $this->hasOne(KinopoiskRating::class, 'filmID', 'filmID');
    }

    /* public function getBigPosterURLAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBigPosterURLAttribute()
    {
        if ($this->attributes['bigPosterURL']) {
            return config('app.url') . '/poster/' . $this->attributes['bigPosterURL'];
        } else {
            return config('app.url') . '/img/original/unnamed.jpg';
        }
    }

    /* public function getRaringAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRatingAttribute()
    {
        $data = json_decode($this->attributes['ratingData']);
        if ($this->ratings()->count()) {
            return (int)$this->ratings->positivePercent;
        } else {
            if (!empty($data->ratingGoodReview)) {
                $data = explode('%', $data->ratingGoodReview);
                return (int)$data[0];
            }
            return 0;
        }

    }

    /* public function getKadrAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getKadrAttribute()
    {
        if ($this->attributes['gallery']) {
            if (!empty($this->attributes['gallery'][0])) {
                $items = json_decode($this->attributes['gallery']);
                $preview = $items[0]->preview;
                $kadr = explode('kadr/sm_', $preview);
                //dd($kadr);
                return config('app.url') . '/kadr/' . $kadr[1];
            }
        } else {
            return config('app.url') . '/img/original/unnamed.jpg';
        }
    }

    /* public function getDirectorAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDirectorAttribute()
    {
        $items = json_decode($this->attributes['creators']);
        return $items[0][0];
    }

    /* public function getActorsAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getActorsAttribute()
    {
        $items = json_decode($this->attributes['creators']);
        return $items[1];
    }

    /* public function comments
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function comments()
    {
        return $this->hasMany(MovieComment::class, 'movie_id', 'id')->whereNull('parent_id');
    }

    /* public function getCommentsStatisticAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCommentsStatisticAttribute()
    {
        $data = [];
        foreach ($this->comments as $comment){
            $data[] = [
                'type' => $comment->type
            ];
        }
        $data = collect($data);
        return [
            'positive' => $data->where('type', 'positive')->count(),
            'neutral' => $data->where('type', 'neutral')->count(),
            'negative' => $data->where('type', 'negative')->count(),
        ];
    }

    /* public function bookm
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function bookMark()
    {

        return $this->hasOne(BookMark::class, 'movie_id', 'id')->where('user_id', !empty($_SERVER['HTTP_U']) ? $_SERVER['HTTP_U'] : null);
    }

    /* public function likes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function likes()
    {
        return $this->hasMany(MovieLike::class, 'movie_id', 'id');
    }

    /* public function views
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function views()
    {
        return $this->hasOne(MovieView::class, 'movie_id', 'id');
    }

    /* public function getVideoHDAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getVideoHDAttribute()
    {
        $data = collect(json_decode($this->getRawOriginal('videoURL')))->toArray();
        return !empty($data['hd']) ? $data['hd'] : null;
    }

    /* public function getVideoURLAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getVideoURLAttribute()
    {
        //dd(config('app.APP_URL'));
        $data['hd'] = url('/').'/storage/video/'.$this->filmID.'.mp4';
        return $data;
    }



}
