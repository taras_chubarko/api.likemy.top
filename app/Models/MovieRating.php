<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieRating extends Model
{
    protected $table = 'movie_rating';

    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
