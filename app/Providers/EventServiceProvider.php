<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\UpdateRatingEvent' => [
            'App\Listeners\UpdateRatingListener',
        ],
        'App\Events\MovieViewEvent' => [
            'App\Listeners\MovieViewListener',
        ],
        'App\Events\MovieLikeEvent' => [
            'App\Listeners\MovieLikeListener',
        ],
        'App\Events\MovieCommentEvent' => [
            'App\Listeners\MovieCommentListener',
        ],
        'App\Events\MovieParseEvent' => [
            'App\Listeners\MovieParseListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
