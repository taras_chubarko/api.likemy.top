<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\MovieCommentLikeType;
use App\Models\MovieCommentLike;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MovieCommentAddLikeMutation extends Mutation
{
    const NAME = 'movieCommentAddLike';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Оценка на отзыв'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieCommentLikeType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_comment_id' => [Type::nonNull(Type::int())],
            'type' => [Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $item = MovieCommentLike::where('user_id', $user->id)->where('movie_comment_id', $args['movie_comment_id'])->first();
        if($item){
            $item->type = $args['type'];
            $item->save();
        }else{
            MovieCommentLike::create([
                'user_id' => $user->id,
                'movie_comment_id' => $args['movie_comment_id'],
                'type' => $args['type'],
            ]);
        }
        $items = MovieCommentLike::where('movie_comment_id', $args['movie_comment_id'])->get();

        return [
            'positive' => $items->where('type', 'positive')->count(),
            'negative' => $items->where('type', 'negative')->count(),
        ];
    }
}
