<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\CollectionType;
use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CollectionAddMutation extends Mutation
{
    const NAME = 'collectionAdd';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Создание коллекции'
    ];

    public function type(): Type
    {
        return GraphQL::type(CollectionType::NAME);
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'name' => ['type' => Type::nonNull(Type::string())],
            'description' => ['type' => Type::string()],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $args['user_id'] = $user->id;

        if($args['id']){
            Collection::where('id', $args['id'])->update($args);
            return Collection::find($args['id']);
        }
        return Collection::create($args);
    }
}
