<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\BookMarkType;
use App\Models\BookMark;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class BookMarkAddMutation extends Mutation
{
    const NAME = 'bookMarkAdd';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Добавить в закладки фильм'
    ];

    public function type(): Type
    {
        return GraphQL::type(BookMarkType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $item = BookMark::where('user_id', $user->id)->whereMovieId($args['movie_id'])->first();
        if($item){
            $item->type = $args['type'];
            $item->save();
        }else{
            $item = BookMark::create([
                'user_id' => $user->id,
                'movie_id' => $args['movie_id'],
                'type' => $args['type'],
            ]);
        }
        return $item;
    }
}
