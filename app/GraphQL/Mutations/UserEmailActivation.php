<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserEmailActivation extends Mutation
{
    protected $attributes = [
        'name' => 'userEmailActivation',
        'description' => 'Активация почты пользователя'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::find($args['id']);
        if($user){
            if(is_null($user->email_verified_at)){
                $user->email_verified_at = Carbon::now();
                $user->save();
                return true;
            }else{
                abort(403, 'Email пользователя уже подтвержден.');
            }
        }else{
            abort(403, 'Такого пользователя не существует.');
        }

    }
}
