<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\TokenDataType;
use App\GraphQL\Types\TokenType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UserLoginMutation extends Mutation
{
    const NAME = 'userLogin';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Авторизация пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::type(TokenDataType::NAME);
    }

    public function args(): array
    {
        return [
            'username' => ['type' => Type::nonNull(Type::string())],
            'password' => ['type' => Type::nonNull(Type::string())],
            'remember_me' => ['type' => Type::nonNull(Type::boolean())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fieldType = filter_var($args['username'], FILTER_VALIDATE_EMAIL) ? 'email' : 'login';
        if(auth()->attempt(array($fieldType => $args['username'], 'password' => $args['password'])))
        {
            $token = Auth::user()->createToken(config('app.name'));
            $token->token->expires_at = $args['remember_me'] ?
                Carbon::now()->addMonth() :
                Carbon::now()->addDay();
            $token->token->save();

            $data = [
                'data' => [
                    'token_type' => 'Bearer',
                    'token' => $token->accessToken,
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
                ],
                'user'=> auth()->user(),
            ];
            return $data;
        }else{
            abort('401', 'Unauthorised');
        }
    }
}
