<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserFollowRequestMutation extends Mutation
{
    protected $attributes = [
        'name' => 'userFollowRequest',
        'description' => 'Подписка на пользователя'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user1 = auth()->user();
        $user2 = User::whereSlug($args['slug'])->first();

        //dd($user1, $user2);
        if($user1->id !== $user2->id){
            $exist = $user1->isFollowing($user2);
            if(!$exist){
                $user1->follow($user2);
                $user1->acceptFollowRequestFrom($user2);
                return true;
            }else{
                abort(500, 'Вы уже подписаны на профиль');
            }
        }else{
            abort(500, 'Вы не можете подписаться на свой профиль');
        }
    }
}
