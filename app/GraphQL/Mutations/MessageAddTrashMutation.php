<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\MessageAction;
use App\Models\MessageStarred;
use App\Models\MessageTrashed;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MessageAddTrashMutation extends Mutation
{
    protected $attributes = [
        'name' => 'messageAddTrash',
        'description' => 'Удаление сообщений'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'messages' => ['type' => Type::listOf(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        foreach ($args['messages'] as $message){
            $item = MessageTrashed::where('message_id',$message)
                ->where('user_id', $user->id)
                ->first();
            if(!$item){
                MessageTrashed::create([
                    'message_id' => $message,
                    'user_id' => $user->id,
                ]);
            }

            $st = MessageStarred::where('user_id', $user->id)->where('message_id',$message)->delete();
        }
        return true;
    }
}
