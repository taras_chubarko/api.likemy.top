<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CollectionRemoveMutation extends Mutation
{
    protected $attributes = [
        'name' => 'collectionRemove',
        'description' => 'Удаление коллекции'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => [Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $item = Collection::find($args['id']);
        $item->items()->delete();
        $item->delete();
        return true;
    }
}
