<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Events\MovieCommentEvent;
use App\Events\MovieLikeEvent;
use App\Events\UpdateRatingEvent;
use App\GraphQL\Types\MovieCommentType;
use App\Models\MovieComment;
use App\Models\MovieLike;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MovieCommentAddMutation extends Mutation
{
    const NAME = 'movieCommentAdd';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Комментарий к фильму'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieCommentType::NAME);
    }

    public function args(): array
    {
        return [
            'comment' => ['type' => Type::nonNull(Type::string())],
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'like' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $like = MovieLike::where('user_id', $user->id)->where('movie_id', $args['movie_id'])->exists();
        if($like){
            $comment = MovieComment::where('user_id', $user->id)->where('movie_id', $args['movie_id'])->exists();
            if(!$comment){
                $comment = MovieComment::create([
                    'user_id' => $user->id,
                    'movie_id' => $args['movie_id'],
                    'comment' => $args['comment'],
                ]);
                event(new MovieCommentEvent($comment));
                //
                if(!empty($args['like']) && !$like){
                    $like = MovieLike::create([
                        'user_id' => $user->id,
                        'movie_id' => $args['movie_id'],
                        'type' => $args['like'],
                    ]);
                    event(new UpdateRatingEvent($like));
                    event(new MovieLikeEvent($like));
                }
                $comments = MovieComment::where('movie_id', $args['movie_id'])
                    ->whereNull('parent_id')
                    ->orderBy('created_at', 'desc')->paginate(20, ['*'], 'page', 1);
                return $comments;
            }
            abort(403, 'Вы уже оставили свой отзыв');
        }
        abort(403, 'Сперва надо оценить фильм');
    }
}
