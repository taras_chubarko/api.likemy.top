<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\MessageAction;
use App\Models\MessageStarred;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class MessageAddStarredMutation extends Mutation
{
    protected $attributes = [
        'name' => 'messageAddStarred',
        'description' => 'Добвить сообщение в помечаные'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'message_id' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $item = MessageStarred::where('message_id', $args['message_id'])
            ->where('user_id', $user->id)
            ->first();
        if(!$item){
            MessageStarred::create([
                'message_id' => $args['message_id'],
                'user_id' => $user->id,
            ]);
            return true;
        }else{
            $item->delete();
            return false;
        }
    }
}
