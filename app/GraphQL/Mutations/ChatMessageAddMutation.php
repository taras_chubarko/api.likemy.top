<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\ChatType;
use App\Events\ChatMessageAddEvent;
use App\Models\Chat;
use App\Models\Message;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class ChatMessageAddMutation extends Mutation
{
    protected $attributes = [
        'name' => 'chatMessageAdd',
        'description' => 'Отправка сообщения в чате'
    ];

    public function type(): Type
    {
        return GraphQL::type(ChatType::NAME);
    }

    public function args(): array
    {
        return [
            'chat_id' => ['type' => Type::nonNull(Type::int())],
            'message' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $sender = auth()->user();

        $chat = Message::find($args['chat_id']);
        $chat->body = $args['message'];
        $chat->read = 0;
        $chat->save();

        $message = Chat::create([
            'user_id' => $sender->id !== $chat->sender_id ? $chat->sender_id : $chat->user_id,
            'sender_id' => $sender->id,
            'chat_id' => $chat->id,
            'message' => $args['message'],
        ]);


        $result['date'] = 'Сегодня';
        $result['messages'] = [
            [
                'id' => $message->id,
                'message' => $message->message,
                'position' => $message->position,
                'time' => $message->time,
                'sender' => [
                    'avatarImage' => $message->sender->avatarImage,
                    'login' => $message->sender->login,
                ],
            ]
        ];
        $ms['chat_id'] = $chat->id;
        $ms['data'] =$result;

        broadcast(new ChatMessageAddEvent($ms));

        return $result;
    }
}
