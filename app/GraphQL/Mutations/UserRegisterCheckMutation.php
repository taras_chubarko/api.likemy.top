<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserRegisterCheckMutation extends Mutation
{
    const NAME = 'userRegisterCheck';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Проверка регистрации пользователя'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'field' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if($args['field'] == 'login'){
            $user = User::where('login', $args['name'])->first();
            if(!$user){
                return true;
            }else{
                abort(422, 'Такой логин уже зарегистрирован!');
            }
        }
        //
        if($args['field'] == 'email'){
            $user = User::where('email', $args['name'])->first();
            if(!$user){
                return true;
            }else{
                abort(422, 'Такой E-mail уже зарегистрирован!');
            }
        }
    }
}
