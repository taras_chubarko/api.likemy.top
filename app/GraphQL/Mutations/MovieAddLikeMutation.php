<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Events\MovieLikeEvent;
use App\Events\UpdateRatingEvent;
use App\GraphQL\Types\MovieType;
use App\Models\MovieLike;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MovieAddLikeMutation extends Mutation
{
    const NAME = 'movieAddLike';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Создание лайка только авторизированым пользователям'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        if($user){
           $userLikeExists = MovieLike::where('user_id', $user->id)->where('movie_id', $args['movie_id'])->exists();
            //dd($userLikeExists);
            if(!$userLikeExists){
                $like = MovieLike::create([
                    'user_id' => $user->id,
                    'movie_id' => $args['movie_id'],
                    'type' => $args['type'],
                ]);
                event(new UpdateRatingEvent($like));
                event(new MovieLikeEvent($like));
                return $like->movie;
            }
            abort(500, 'changeLike');
        }
        abort(403, 'Нужна авторизация');
    }
}
