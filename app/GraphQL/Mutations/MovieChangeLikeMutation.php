<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Events\MovieLikeEvent;
use App\Events\UpdateRatingEvent;
use App\GraphQL\Types\MovieType;
use App\Models\MovieLike;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MovieChangeLikeMutation extends Mutation
{
    const NAME = 'movieChangeLike';

    protected $attributes = [
        'name' => 'movieChangeLike',
        'description' => 'Изминение лайка'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        if($user){
            $like = MovieLike::where('user_id', $user->id)->where('movie_id', $args['movie_id'])->first();
            if($like){
                $like->type = $args['type'];
                $like->save();
                event(new UpdateRatingEvent($like));
                event(new MovieLikeEvent($like));
                return $like->movie;
            }
            abort(500, 'Ваш лайк не доступен или небыл добавлен.');
        }
        abort(403, 'Нужна авторизация');
    }
}
