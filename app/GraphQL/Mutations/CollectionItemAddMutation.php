<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\CollectionType;
use App\Models\CollectionItem;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CollectionItemAddMutation extends Mutation
{
    const NAME = 'collectionItemAdd';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Добавление фильма в коллекцию'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(CollectionType::NAME));
    }

    public function args(): array
    {
        return [
            'collection_id' => ['type' => Type::nonNull(Type::int())],
            'movie_id' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        CollectionItem::updateOrCreate($args);

        return $user->collections;
    }
}
