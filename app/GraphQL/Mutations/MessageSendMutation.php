<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Inputs\MessageSendInput;
use App\Models\Chat;
use App\Models\Message;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MessageSendMutation extends Mutation
{
    protected $attributes = [
        'name' => 'messageSend',
        'description' => 'Отправка сообщения'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'data' => ['type' => GraphQL::type(MessageSendInput::NAME)]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $sender = auth()->user();
        $data = $args['data'];
        //$data['sender_id'] = $sender->id;
        //$chat = Message::where('type', 'chat')->where('user_id', $sender->id)->orWhere('sender_id', $sender->id)->first();

        $chat = Message::query()
            ->where('type', 'chat')
            ->where('user_id', $data['user_id'])
            ->where('sender_id', $sender->id)
            ->orWhere(function ($q) use ($sender, $data) {
                $q->where('user_id', $sender->id);
                $q->where('sender_id', $data['user_id']);
            })->first();

        if($chat){
            $chat->body = $data['message'];
            $chat->read = 0;
            $chat->save();
        }else{
            $chat = new Message;
            
            $chat->user_id = $data['user_id'];
            $chat->sender_id = $sender->id;
            $chat->type = 'chat';
            $chat->body = $data['message'];
            $chat->read = 0;

            $chat->save();

            // $chat =  Message::create([
            //     'user_id' => $data['user_id'],
            //     'sender_id' => $sender->id,
            //     'type' => 'chat',
            //     'body' => $data['message'],
            // ]);
        }

        $chat_message = new Chat;

        $chat_message->user_id = $data['user_id'];
        $chat_message->sender_id = $sender->id;
        $chat_message->chat_id = $chat->id;
        $chat_message->message = $data['message'];

        $chat_message->save();

        // Chat::create([
        //     'user_id' => $data['user_id'],
        //     'sender_id' => $sender->id,
        //     'chat_id' => $chat->id,
        //     'message' => $data['message'],
        // ]);

        return true;
    }
}
