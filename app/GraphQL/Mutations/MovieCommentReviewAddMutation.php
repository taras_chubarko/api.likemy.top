<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\MovieCommentType;
use App\Models\MovieComment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class MovieCommentReviewAddMutation extends Mutation
{
    const NAME = 'movieCommentReviewAdd';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Ответ на отзыв'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieCommentType::NAME);
    }

    public function args(): array
    {
        return [
            'parent_id' => ['type' => Type::nonNull(Type::int())],
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'comment' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $review = MovieComment::create([
            'user_id' => $user->id,
            'movie_id' => $args['movie_id'],
            'parent_id' => $args['parent_id'],
            'comment' => $args['comment'],
        ]);
        return $review;
    }
}
