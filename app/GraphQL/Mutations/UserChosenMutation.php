<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\User;
use App\Models\UserChosen;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserChosenMutation extends Mutation
{
    protected $attributes = [
        'name' => 'userChosen',
        'description' => 'Избранные подписки'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user1 = auth()->user();
        $user2 = User::whereSlug($args['slug'])->first();
        if($user1->id !== $user2->id) {
            if (!UserChosen::where('user_id', $user1->id)->where('chosen_id', $user2->id)->exists()) {
                UserChosen::create([
                    'user_id' => $user1->id,
                    'chosen_id' => $user2->id,
                ]);
                return true;
            } else {
                abort(500, 'Пользователь уже в избранном');
            }
        }else{
            abort(500, 'Вы не можете добавить себя в избранное.');
        }
    }
}
