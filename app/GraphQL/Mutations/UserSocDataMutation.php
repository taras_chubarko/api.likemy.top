<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\UserSocDataType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Ixudra\Curl\Facades\Curl;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UserSocDataMutation extends Mutation
{
    protected $attributes = [
        'name' => 'userSocData',
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserSocDataType::NAME);
    }

    public function args(): array
    {
        return [
            'network' => ['type' => Type::string()],
            'code' => ['type' => Type::string()],
            'redirect_uri' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return $this->{$args['network']}($args);
    }

    /* public function facebook
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function facebook($args)
    {
        $response = Curl::to('https://graph.facebook.com/oauth/access_token')
            ->withData([
                'client_id' => config('services.social.facebook.client_id'),
                'redirect_uri' => $args['redirect_uri'],
                'client_secret' => config('services.social.facebook.client_secret'),
                'code' => $args['code'],
            ])
            ->asJson()
            ->get();
        $me = Curl::to('https://graph.facebook.com/me')
            ->withData([
                'access_token' => $response->access_token,
                'fields' => 'name,first_name,last_name,email,id'
            ])
            ->asJson()
            ->get();
        $me = collect($me)->toArray();
        $me['picture'] = 'https://graph.facebook.com/' . $me['id'] . '/picture?width=500&heigth=500';
        return $me;
    }

    /* public function yandex
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function yandex($args)
    {
        $params = array(
            'grant_type'    => 'authorization_code',
            'code'          => $args['code'],
            'client_id'     => config('services.social.yandex.client_id'),
            'client_secret' => config('services.social.yandex.client_secret'),
        );

        $ch = curl_init('https://oauth.yandex.ru/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($data, true);

        //if (!empty($data['access_token'])) {
            // Токен получили, получаем данные пользователя.
            $ch = curl_init('https://login.yandex.ru/info');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('format' => 'json'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: OAuth ' . $data['access_token']));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $info = curl_exec($ch);
            curl_close($ch);

            $info = json_decode($info, true);
           // var_dump($info);
        //}

        $item = [
            'id' => $info['id'],
            'name' => $info['real_name'],
            'first_name' => $info['first_name'],
            'last_name' => $info['last_name'],
            'email' => $info['default_email'],
            'picture' => null,
        ];
        return $item;
    }

    /* public function google
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function google($args)
    {

        $response = Curl::to('https://accounts.google.com/o/oauth2/token')
            ->withData([
                'client_id' => config('services.social.google.client_id'),
                'redirect_uri' => $args['redirect_uri'],
                'client_secret' => config('services.social.google.client_secret'),
                'code' => $args['code'],
                'grant_type' => 'authorization_code',
            ])
            ->asJson()
            ->post();


        $me = Curl::to('https://www.googleapis.com/oauth2/v1/userinfo')
            ->withData([
                'access_token' => $response->access_token,
            ])
            ->asJson()
            ->get();
        $me = collect($me)->toArray();

        $data = [
            'id' => $me['id'],
            'name' => $me['name'],
            'first_name' => $me['given_name'],
            'last_name' => $me['family_name'],
            'email' => $me['email'],
            'picture' => $me['picture'],
        ];
        return $data;
    }

    /* public function mailru
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function mailru($args)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://oauth.mail.ru/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&redirect_uri=" . $args['redirect_uri'] . "&code=" . $args['code'],
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic MzM2YjIxMDhkMmIzNGFiYmI1OWVmMDJhMWM1NDM0NWU6ZGI5ZmRmZmIwNmM3NGU3NDgxYTZlOWMzODBjNWUzNzI=",
                "Content-Type: application/x-www-form-urlencoded",
                "User-Agent:Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response);

        $me = Curl::to('https://oauth.mail.ru/userinfo')
            ->withData([
                'access_token' => $response->access_token,
            ])
            ->asJson()
            ->get();
        $me = collect($me)->toArray();

        $data = [
            'id' => $me['id'],
            'name' => $me['name'],
            'first_name' => $me['first_name'],
            'last_name' => $me['last_name'],
            'email' => $me['email'],
            'picture' => $me['image'],
        ];
        return $data;
    }

    /* public function vkontakte
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function vkontakte($args)
    {
        $response = Curl::to('https://oauth.vk.com/access_token')
            ->withData([
                'client_id' => config('services.social.vkontakte.client_id'),
                'redirect_uri' => $args['redirect_uri'],
                'client_secret' => config('services.social.vkontakte.client_secret'),
                'code' => $args['code'],
            ])
            ->asJson()
            ->get();

        $me = Curl::to('https://api.vk.com/method/users.get')
            ->withData([
                'access_token' => $response->access_token,
                'fields' => 'uid,first_name,email,last_name,screen_name,photo_max_orig',
                'user_id' => $response->user_id,
                'v' => 5.52
            ])
            ->asJson()
            ->get();
        $me = collect($me)->toArray();
        //var_dump($me);
        $me = $me['response'][0];

        $data = [
            'id' => $me->id,
            'name' => $me->first_name . ' ' . $me->last_name,
            'first_name' => $me->first_name,
            'last_name' => $me->last_name,
            'email' => !empty($me->email) ? $me->email : null,
            'picture' => $me->photo_max_orig,
        ];
        return $data;
    }
}
