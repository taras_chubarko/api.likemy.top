<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\TokenDataType;
use App\Models\User;
use App\Models\UserSocial;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UserSocAuthMutation extends Mutation
{
    const NAME = 'userSocAuth';

    protected $attributes = [
        'name' => 'userSocAuth',
        'description' => 'Авторизация через социальные сети'
    ];

    public function type(): Type
    {
        return GraphQL::type(TokenDataType::NAME);
    }

    public function args(): array
    {
        return [
            'data' => ['type' => Type::nonNull(Type::string())],
            'network' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = json_decode($args['data']);
        $user = User::where('email', $data->email)->first();
        if ($user) {
            $soc = UserSocial::where('user_id', $user->id)->where('network', $args['network'])->first();
            if (!$soc) {
                $sp = new UserSocial(['network' => $args['network'], 'data' => $data]);
                $user->socialProfiles()->save($sp);
            }
            return $this->auth($user);
        } else {
            return $this->register($data, $args['network']);
        }
    }

    /* public function auth
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function auth($user)
    {
        $token = Auth::loginUsingId($user->id)->createToken(config('app.name'));
        $token->token->expires_at = Carbon::now()->addDay();
        $token->token->save();

        $data = [
            'data' => [
                'token_type' => 'Bearer',
                'token' => $token->accessToken,
                'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
            ],
            'user' => auth()->user(),
        ];
        return $data;
    }

    /* public function register
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function register($data, $network)
    {
        $item['password'] = bcrypt(123456789);
        $item['remember_token'] = Str::random(10);
        $item['email'] = $data->email;
        $item['name'] = $data->first_name;
        $item['surname'] = $data->last_name;
        $item['login'] = Str::slug($data->name, '.');

        if ($data->picture) {
            $pathAva = storage_path() . '/app/public/avatar/' . Str::random(20) . '.jpg';
            \Image::make($data->picture)->save($pathAva);
            $imAva = pathinfo($pathAva);
            $imAva['dirname'] = 'public/avatar';
            $item['avatar'] = $imAva;
        }

        $user = User::create($item);
        $sp = new UserSocial(['network' => $network, 'data' => $data]);
        $user->socialProfiles()->save($sp);

        return $this->auth($user);
    }


}
