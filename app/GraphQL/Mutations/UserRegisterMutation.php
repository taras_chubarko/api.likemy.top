<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\GraphQL\Types\TokenDataType;
use App\Mail\RegisterMail;
use App\Models\User;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UserRegisterMutation extends Mutation
{
    const NAME = 'userRegister';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Регистрация пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::type(TokenDataType::NAME);
    }

    public function args(): array
    {
        return [
            'login' => ['type' => Type::nonNull(Type::string())],
            'email' => ['type' => Type::nonNull(Type::string())],
            'password' => ['type' => Type::nonNull(Type::string())],
            'password_confirmation' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
//        if(filter_var($args['username'], FILTER_VALIDATE_EMAIL)) {
//            if($this->emailExist($args['username'])){
//                return $this->register($args);
//            }
//        }else{
//            if($this->loginExist($args['username'])){
//                return $this->register($args);
//            }
//        }

        if ($this->emailExist($args['email'])) {
            return $this->register($args);
        }
    }

    /* public function loginExist
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function loginExist($login)
    {
        $user = User::where('login', $login)->exists();
        if (!$user) {
            return true;
        } else {
            abort(422, 'Пользователь с таким именем уже зарегистрирован.');
        }
    }

    /* public function emailExist
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function emailExist($email)
    {
        $user = User::where('email', $email)->exists();
        if (!$user) {
            return true;
        } else {
            abort(422, 'Пользователь с таким E-mail уже зарегистрирован.');
        }
    }

    /* public function register
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function register($args)
    {

        $data['password'] = bcrypt($args['password']);
        $data['remember_token'] = Str::random(10);
        $data['login'] = $args['login'];
        $data['email'] = $args['email'];
//        if (filter_var($args['login'], FILTER_VALIDATE_EMAIL)) {
//            $login = explode('@', $args['login']);
//            $data['login'] = $login[0];
//            $data['email'] = $args['login'];
//        } else {
//            $data['login'] = $args['login'];
//        }
        $user = User::create($data);

        \Mail::to($data['email'])->send(new RegisterMail($user));

        //$fieldType = filter_var($args['login'], FILTER_VALIDATE_EMAIL) ? 'email' : 'login';
        if (auth()->attempt(array('email' => $args['email'], 'password' => $args['password']))) {
            $token = Auth::user()->createToken(config('app.name'));
            $token->token->expires_at = Carbon::now()->addDay();
            $token->token->save();

            $data = [
                'data' => [
                    'token_type' => 'Bearer',
                    'token' => $token->accessToken,
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
                ],
                'user' => auth()->user(),
            ];
            return $data;
        } else {
            abort('401', 'Unauthorised');
        }
    }
}
