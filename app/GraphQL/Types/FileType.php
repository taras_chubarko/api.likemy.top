<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;

class FileType extends GraphQLType
{
    const NAME = 'File';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'dirname' => ['type' => Type::string()],
            'basename' => ['type' => Type::string()],
            'extension' => ['type' => Type::string()],
            'filename' => ['type' => Type::string()],
        ];
    }
}
