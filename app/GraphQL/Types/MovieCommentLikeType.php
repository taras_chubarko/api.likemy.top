<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieCommentLikeType extends GraphQLType
{
    const NAME = 'MovieCommentLike';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'positive' => ['type' => Type::int()],
            'negative' => ['type' => Type::int()],
        ];
    }
}
