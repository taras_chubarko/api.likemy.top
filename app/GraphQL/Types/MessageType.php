<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Carbon\Carbon;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MessageType extends GraphQLType
{
    const NAME = 'Message';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'type' => ['type' => Type::string()],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
            'sender' => ['type' => GraphQL::type(UserType::NAME)],
            'subject' => ['type' => Type::string()],
            'parent_id' => ['type' => Type::int()],
            'owner_id' => ['type' => Type::int()],
            'body' => ['type' => Type::string()],
            'bodyShort' => ['type' => Type::string()],
            'starred' => ['type' => Type::boolean()],
            'trash' => ['type' => Type::boolean()],
            'read' => ['type' => Type::boolean()],
            'created_at' => ['type' => Type::string(), 'args' => [ 'format' => ['type' => Type::string()]]],
            'updated_at' => ['type' => Type::string(), 'args' => [ 'format' => ['type' => Type::string()]]],
        ];
    }

    /* public function resolveCreatedAtField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveCreatedAtField($root, $args)
    {
        return Carbon::parse($root->created_at)->locale('ru')->isoFormat($args['format']);
    }

    /* public function resolveCreatedAtField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveUpdatedAtField($root, $args)
    {
        return Carbon::parse($root->updated_at)->locale('ru')->isoFormat($args['format']);
    }


}
