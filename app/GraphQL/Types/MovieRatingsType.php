<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieRatingsType extends GraphQLType
{
    const NAME = 'MovieRatings';

    protected $attributes = [
        'name' => 'MovieRatings',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'negativeCount' => ['type' => Type::int()],
            'positiveCount' => ['type' => Type::int()],
            'neutralCount' => ['type' => Type::int()],
            'totalCount' => ['type' => Type::int()],
            'negativePercent' => ['type' => Type::float()],
            'positivePercent' => ['type' => Type::float()],
            'neutralPercent' => ['type' => Type::float()],
            'totalPercent' => ['type' => Type::float()],
        ];
    }

}
