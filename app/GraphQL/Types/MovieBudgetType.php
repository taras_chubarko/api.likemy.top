<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieBudgetType extends GraphQLType
{
    const NAME = 'MovieBudget';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'budget' => ['type' => Type::string()],
            'grossRU' => ['type' => Type::string()],
            'grossUSA' => ['type' => Type::string()],
            'grossWorld' => ['type' => Type::string()],
            'marketing' => ['type' => Type::string()],
        ];
    }
}
