<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UsePreferencesType extends GraphQLType
{
    const NAME = 'UsePreferences';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'total' => ['type' => Type::int()],
            'genre' => ['type' => Type::listOf(GraphQL::type(UsePreferencesDataType::NAME))],
            'county' => ['type' => Type::listOf(GraphQL::type(UsePreferencesDataType::NAME))],
            'year' => ['type' => Type::listOf(GraphQL::type(UsePreferencesYearsType::NAME))],
            'actors' => ['type' => Type::listOf(GraphQL::type(MoviePeopleType::NAME))],
        ];
    }
}
