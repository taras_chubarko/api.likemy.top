<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TokenType extends GraphQLType
{

    const NAME = 'Token';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'token_type' => ['type' => Type::string()],
            'token' => ['type' => Type::string()],
            'expires_at' => ['type' => Type::string()],
        ];
    }
}
