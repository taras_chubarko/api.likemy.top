<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MoviePremierType extends GraphQLType
{
    const NAME = 'MoviePremier';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'premiereRU' => ['type' => Type::string()],
            'premiereWorld' => ['type' => Type::string()],
        ];
    }
}
