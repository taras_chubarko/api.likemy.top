<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\MovieComment;
use App\Models\MovieLike;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieCommentType extends GraphQLType
{
    const NAME = 'MovieComment';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'parent_id' => ['type' => Type::int()],
            'comment' => ['type' => Type::string()],
            'dt' => ['type' => Type::string()],
            'user' => ['type' =>GraphQL::type(UserType::NAME)],
            'children' => ['type' => Type::listOf(GraphQL::type(MovieCommentType::NAME))],
            'type' => ['type' => Type::string()],
            'likes' => ['type' => GraphQL::type(MovieCommentLikeType::NAME)],
            'movie' => ['type' => GraphQL::type(MovieType::NAME)],
        ];
    }

    /* public function resolveTypeField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveTypeField($root)
    {
        $item = MovieLike::where('user_id', $root->user_id)->where('movie_id', $root->movie_id)->first();
        return $item->type;
    }
}
