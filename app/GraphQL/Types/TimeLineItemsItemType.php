<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TimeLineItemsItemType extends GraphQLType
{
    const NAME = 'TimeLineItemsItem';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'movie' =>['type' => GraphQL::type(MovieType::NAME)],
            'like' =>['type' => GraphQL::type(MovieLikeType::NAME)],
            'comment' =>['type' => GraphQL::type(MovieCommentType::NAME)],
        ];
    }
}
