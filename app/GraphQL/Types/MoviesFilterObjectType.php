<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MoviesFilterObjectType extends GraphQLType
{

    const NAME = 'MoviesFilterObject';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'genre' => ['type' => Type::listOf(GraphQL::type(MoviesFilterType::NAME))],
            'country' => ['type' => Type::listOf(GraphQL::type(MoviesFilterType::NAME))],
        ];
    }
}
