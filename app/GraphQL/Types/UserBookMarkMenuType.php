<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserBookMarkMenuType extends GraphQLType
{
    const NAME = 'UserBookMarkMenu';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => Type::string()],
            'count' => ['type' => Type::int()],
            'component' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'separator' => ['type' => Type::boolean()],
        ];
    }
}
