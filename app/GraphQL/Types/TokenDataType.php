<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class TokenDataType extends GraphQLType
{
    const NAME = 'TokenData';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'data' => ['type' => GraphQL::type(TokenType::NAME)],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
        ];
    }
}
