<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Carbon\Carbon;
use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;

class UserType extends GraphQLType
{

    const NAME = 'User';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'name' => ['type' => Type::string()],
            'surname' => ['type' => Type::string()],
            'login' => ['type' => Type::string()],
            'email' => ['type' => Type::string()],
            'gender' => ['type' => Type::string()],
            'birthday' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'avatarImage' => ['type' => Type::string()],
            'bgImage' => ['type' => Type::string()],
            'likes_count' => ['type' => Type::int()],
            'movie_comments_count' => ['type' => Type::int()],
            'ifollow' => ['type' => Type::boolean()],
            'percent_degree' => ['type' => Type::int()],
            'unreadMessages' => ['type' => Type::int()],
            'inFollowers' => ['type' => Type::boolean()],
            'coincidence' => ['type' => Type::string()],
            'last_activity' => ['type' => Type::string(), 'args' => [ 'format' => ['type' => Type::string()]]],
            'created_at' => ['type' => Type::string(), 'args' => [ 'format' => ['type' => Type::string()]]],
            'updated_at' => ['type' => Type::string(), 'args' => [ 'format' => ['type' => Type::string()]]],
        ];
    }

    public function resolveLastActivityField($root, $args)
    {
        if ($root->last_activity != null) {
            return Carbon::parse($root->last_activity)->locale('ru')->isoFormat($args['format']);
        } else {
            return null;
        }
    }

    public function resolveCreatedAtField($root, $args)
    {
        return Carbon::parse($root->created_at)->locale('ru')->isoFormat($args['format']);
    }

    public function resolveUpdatedAtField($root, $args)
    {
        return Carbon::parse($root->updated_at)->locale('ru')->isoFormat($args['format']);
    }
}
