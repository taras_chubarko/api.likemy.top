<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Classes\KinoPoisk;
use App\Models\Movie;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MoviePeopleType extends GraphQLType
{
    const NAME = 'MoviePeople';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'type' => ['type' => Type::string()],
            'nameRU' => ['type' => Type::string()],
            'nameEN' => ['type' => Type::string()],
            'posterURL' => ['type' => Type::string()],
            'professionText' => ['type' => Type::string()],
            'professionKey' => ['type' => Type::string()],
            'birthday' => ['type' => Type::string()],
            'age' => ['type' => Type::string()],
            'birthplace' => ['type' => Type::string()],
            'profession' => ['type' => Type::string()],
            'genreStatistic' => ['type' => Type::listOf(GraphQL::type(GenreStatisticType::NAME))],
        ];
    }

    /* public function resolvePosterURLField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolvePosterURLField($root, $args)
    {
        $root = collect($root)->toArray();
        $id = !empty($root['id']) ? $root['id'] : $root['peopleID'];
        return config('app.url').'/poster/actor_iphone/iphone360_'.$id.'.jpg';
    }

    /* public function resolveGenreStatisticField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveGenreStatisticField($root, $args)
    {
        $root = collect($root)->toArray();
        $id = !empty($root['id']) ? $root['id'] : $root['peopleID'];

        $actor = KinoPoisk::getActor($id);
        $filmography = collect($actor->filmography)->collapse()->pluck('filmID');
        $movies = Movie::whereIn('filmID', $filmography)->select('genre')->get();
        $data = [];
        if($movies){
            $genres = $movies->pluck('genre');

            $arr = [];
            foreach ($genres as $item){
                $arr[] = explode(',', preg_replace('/\s+/', '', $item));
            }
            $arr = collect($arr)->collapse();
            $un = $arr->unique()->values();

            foreach ($un as $item){

                $count = $arr->filter(function ($value, $key) use ($item) {
                    return $value == $item;
                })->count();


                $data[] = [
                    'name' => $item,
                    'percent' => (int) round(($count / $movies->count()) * 100),
                ];
            }
        }
        $data = collect($data)->sortByDesc('percent')->values();
        return $data;
    }
}
