<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieVideoURLType extends GraphQLType
{
    const NAME = 'MovieVideoURL';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'hd' => ['type' => Type::string()],
            'sd' => ['type' => Type::string()],
            'low' => ['type' => Type::string()],
        ];
    }

//    /* public function resolveHdField
//     * @param
//     *-----------------------------------
//     *|
//     *-----------------------------------
//     */
//    public function resolveHdField($root, $args)
//    {
//        dd($root);
//        return config('app.APP_URL').'storage/video/'.$root->filmID.'.mp4';
//    }
}
