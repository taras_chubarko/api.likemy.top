<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TimeLineType extends GraphQLType
{
    const NAME = 'TimeLine';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'date' => ['type' => Type::string()],
            'items' => ['type' => Type::listOf(GraphQL::type(TimeLineItemsType::NAME))],
        ];
    }
}
