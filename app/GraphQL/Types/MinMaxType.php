<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MinMaxType extends GraphQLType
{
    const NAME = 'MinMax';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'min' => ['type' => Type::int()],
            'max' => ['type' => Type::int()],
        ];
    }
}
