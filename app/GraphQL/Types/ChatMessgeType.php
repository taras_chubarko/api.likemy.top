<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ChatMessgeType extends GraphQLType
{
    const NAME = 'ChatMessge';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'message' => ['type' => Type::string()],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
            'sender' => ['type' => GraphQL::type(UserType::NAME)],
            'position' => ['type' => Type::string()],
            'time' => ['type' => Type::string()],
        ];
    }
}
