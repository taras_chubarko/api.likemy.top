<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserSocDataType extends GraphQLType
{
    const NAME = 'UserSocData';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'first_name' => ['type' => Type::string()],
            'last_name' => ['type' => Type::string()],
            'email' => ['type' => Type::string()],
            'picture' => ['type' => Type::string()],
        ];
    }
}
