<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieInMainType extends GraphQLType
{
    const  NAME = 'MovieInMain';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'tags' => ['type' => Type::listOf(GraphQL::type(MovieTagsType::NAME))],
            'items' => ['type' => Type::listOf(GraphQL::type(MovieType::NAME))],
            'info' => ['type' => Type::int()]
        ];
    }
}
