<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CollectionType extends GraphQLType
{
    const NAME = 'Collection';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'name' => ['type' => Type::string()],
            'description' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'countItems' => ['type' => Type::int()],
            'posterImage' => ['type' => Type::string()],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
        ];
    }
}
