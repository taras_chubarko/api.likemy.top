<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserCollectionItemsType extends GraphQLType
{
    const NAME = 'UserCollectionItems';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'collection' => ['type' => GraphQL::type(CollectionType::NAME)],
            'items' => ['type' => GraphQL::paginate(MovieType::NAME)],
        ];
    }
}
