<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UsePreferencesYearsType extends GraphQLType
{
    const NAME = 'UsePreferencesYears';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'labels' => ['type' => Type::listOf(Type::int())],
            'percent' => ['type' => Type::int()],
        ];
    }
}
