<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MovieType extends GraphQLType
{
    const NAME = 'Movie';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int()],
            'filmID' => ['type' => Type::int()],
            'nameRU' => ['type' => Type::string()],
            'nameEN' => ['type' => Type::string()],
            'bigPosterURL' => ['type' => Type::string()],
            'kadr' => ['type' => Type::string()],
            'country' => ['type' => Type::string()],
            'genre' => ['type' => Type::string()],
            'year' => ['type' => Type::string()],
            'slogan' => ['type' => Type::string()],
            'rating' => ['type' => Type::int()],
            'description' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'videoURL' => ['type' => GraphQL::type(MovieVideoURLType::NAME)],
            'director' => ['type' => GraphQL::type(MoviePeopleType::NAME)],
            'actors' => ['type' => Type::listOf(GraphQL::type(MoviePeopleType::NAME))],
            'ratings' => ['type' => GraphQL::type(MovieRatingsType::NAME)],
            'comments_statistic' => ['type' => GraphQL::type(MovieCommentStatisticType::NAME)],
            'bookMark' => ['type' => GraphQL::type(BookMarkType::NAME)],
            'ratingAgeLimits' => ['type' => Type::string()],
            'budgetData' => ['type' => GraphQL::type(MovieBudgetType::NAME)],
            'rentData' => ['type' => GraphQL::type(MoviePremierType::NAME)],
        ];
    }

    /* public function resolveNegativePercentField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveRatingsField($root, $args)
    {
        if(!$root->ratings){
            return [
                'negativePercent' => 0,
                'positivePercent' => 0,
                'neutralPercent' => 0,
            ];
        }else{
            return $root->ratings;
        }
    }
}
