<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;

class UserFollowersType extends GraphQLType
{
    const NAME = 'UserFollowers';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => Type::string()],
            'image' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'likes' => ['type' => Type::int()],
            'reviews' => ['type' => Type::int()],
            'last_activity' => ['type' => Type::string()],
        ];
    }
}
