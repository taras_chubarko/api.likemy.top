<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TimeLineItemsType extends GraphQLType
{
    const NAME = 'TimeLineItems';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'user' => ['type' => GraphQL::type(UserType::NAME)],
            'count' => ['type' => Type::int()],
            'movies' => ['type' => Type::listOf(GraphQL::type(TimeLineItemsItemType::NAME))],
        ];
    }
}
