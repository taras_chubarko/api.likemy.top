<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\CollectionType;
use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserCollectionsQuery extends Query
{
    const NAME = 'userCollections';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Коллекции пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(CollectionType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        $q = Collection::query();
        $q->where('user_id', $user->id);
        $q->orderBy('created_at', 'desc');
        $collections = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $collections;
    }
}
