<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MoviePeopleType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieActorsQuery extends Query
{
    const NAME = 'movieActors';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Актеры фильма'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MoviePeopleType::NAME));
    }

    public function args(): array
    {
        return [
            'filmID' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        //return Movie::where('filmID', $args['filmID'])->first()->actors;
        return KinoPoisk::getActors($args['filmID']);
    }
}
