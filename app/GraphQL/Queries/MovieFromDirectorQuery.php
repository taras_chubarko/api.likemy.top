<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieFromDirectorQuery extends Query
{
    const NAME = 'movieFromDirector';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Фильмы от режисера'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieType::NAME));
    }

    public function args(): array
    {
        return [
            'peopleID' => ['type' => Type::nonNull(Type::int())],
            'filmID' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $ids = KinoPoisk::getFromDirectorFilmsID($args['peopleID']);
        $filtered = $ids->filter(function ($value, $key) use ($args) {
            return $value != $args['filmID'];
        });

        return Movie::whereIn('filmID', $filtered)->get();
    }
}
