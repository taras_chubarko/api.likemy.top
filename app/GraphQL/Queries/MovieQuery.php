<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Events\MovieViewEvent;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use App\Models\MovieView;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MovieQuery extends Query
{
    const NAME = 'movie';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Фильм'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $movie = Movie::where('slug', $args['slug'])->first();
        event(new MovieViewEvent($movie));
        return $movie;
    }
}
