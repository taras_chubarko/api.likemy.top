<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Inputs\UseLikesMovieItput;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserLikesMovieQuery extends Query
{
    protected $attributes = [
        'name' => 'userLikesMovie',
        'description' => 'Фильмы лайкнутые пользователем  - вывод в профиле'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'filter' => ['type' => GraphQL::type(UseLikesMovieItput::NAME)],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();
        $likes = $user->likes->pluck('movie_id');
        $data = $args['filter'];

        $q = Movie::query();
        $q->whereIn('id', $likes);

        if (isset($data['type']) && $data['type'] !== 'all') {
            $type = $data['type'];
            $q->whereHas('likes', function($l) use ($type, $user){
                $l->where('type', $type);
                $l->where('user_id', $user->id);
            });
        }

        if (isset($data['genre'])) {
            $genre = $data['genre'];
            $q->where(function ($g) use ($genre) {
                foreach ($genre as $datum) {
                    $g->orWhere('genre', 'LIKE', '%' . $datum . '%');
                }
            });
        }

        if (isset($data['country'])) {
            $country = $data['country'];
            $q->where(function ($g) use ($country) {
                foreach ($country as $datum) {
                    $g->orWhere('country', 'LIKE', '%' . $datum . '%');
                }
            });
        }

        if($data['year']['from'] && $data['year']['to']){
            $q->whereBetween('year', [$data['year']['from'],$data['year']['to']]);
        }

        $movies = $q->paginate($data['per_page'], ['*'], 'page', $data['current_page']);

        //dd($movies);
        //echo '<pre>'; var_dump($movies); echo '</pre>';

        return $movies;
    }
}
