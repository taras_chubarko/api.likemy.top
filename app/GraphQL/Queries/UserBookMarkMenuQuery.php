<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserBookMarkMenuType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserBookMarkMenuQuery extends Query
{
    const NAME = 'userBookMarkMenu';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Меню закладок пользователя'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(UserBookMarkMenuType::NAME));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        $data[] = [
            'name' => 'Буду смотреть',
            'count' => $user->bookmarks->where('type', 'be')->count(),
            'component' => 'UserCollectionsBook',
            'type' => 'be',
            'separator' => false,
        ];

        $data[] = [
            'name' => 'Не буду смотреть',
            'count' => $user->bookmarks->where('type', 'nobe')->count(),
            'component' => 'UserCollectionsBook',
            'type' => 'nobe',
            'separator' => false,
        ];

        $data[] = [
            'name' => 'Смотрю',
            'count' => $user->bookmarks->where('type', 'view')->count(),
            'component' => 'UserCollectionsBook',
            'type' => 'view',
            'separator' => false,
        ];

        $data[] = [
            'name' => 'Просмотрено',
            'count' => $user->bookmarks->where('type', 'viewed')->count(),
            'component' => 'UserCollectionsBook',
            'type' => 'viewed',
            'separator' => true,
        ];

//        $data[] = [
//            'name' => 'Подборки',
//            'count' => $user->collections->count(),
//            'component' => 'UserCollectionsCollection',
//            'type' => null,
//            'separator' => true,
//        ];

        return $data;
    }
}
