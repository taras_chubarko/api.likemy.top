<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserProfileTabsType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UserProfileTabsQuery extends Query
{
    const NAME = 'userProfileTabs';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Вкладки профиля'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(UserProfileTabsType::NAME));
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $data = [
            [
                'name' => 'Предпочтения',
                'path' => '/user/'.$user->slug,
            ],
            [
                'name' => 'Оценки ('.$user->likes->count().')',
                'path' => '/user/'.$user->slug.'/likes',
            ],
            [
                'name' => 'Отзывы ('.$user->movieComments->count().')',
                'path' => '/user/'.$user->slug.'/reviews',
            ],
            [
                'name' => 'Подписчики ('.$user->followers->count().')',
                'path' => '/user/'.$user->slug.'/followers',
            ],
            [
                'name' => 'Подписки ('.$user->followings->count().')',
                'path' => '/user/'.$user->slug.'/followings',
            ],
        ];
        return $data;
    }
}
