<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MoviesFilterObjectType;
use App\Models\MovieFilter;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MoviesFilterQuery extends Query
{
    const NAME = 'moviesFilter';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Фильтры для фильмов'
    ];

    public function type(): Type
    {
        return GraphQL::type(MoviesFilterObjectType::NAME);
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return MovieFilter::first();
    }
}
