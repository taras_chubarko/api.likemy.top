<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MessageType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MessagesSentQuery extends Query
{
    protected $attributes = [
        'name' => 'messagesSent',
        'description' => 'Отправленные сообщения'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MessageType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'search' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();
        $q = $user->sent();
        $q->doesntHave('inTrash');
        if(!empty($args['search'])){
            $q->where('subject', 'like', '%'.$args['search'].'%');
            $q->orWhere('body', 'like', '%'.$args['search'].'%');
        }
        $q->orderBy('created_at', 'desc');
        $items = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $items;
    }
}
