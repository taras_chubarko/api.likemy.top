<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MessageType;
use App\Models\Message;
use App\Models\MessageAction;
use App\Models\MessageTrashed;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MessagesChatsQuery extends Query
{
    protected $attributes = [
        'name' => 'messagesChats',
        'description' => 'Чаты'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MessageType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'search' => ['type' => Type::string()],
        ];
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        $authUser = auth()->user();
        return $authUser != null && $authUser->slug == $args['slug'];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $trashedIds = MessageTrashed::where('user_id', $user->id)->get()->pluck('message_id')->toArray();

        $q = Message::query();
        //$q->whereIn('id', $trashedIds, 'and', true);
       // $q->without('inTrash');
        $q->whereNotIn('id', $trashedIds);

        //$q->where('type', 'chat')->where('user_id', $user->id)->orWhere('sender_id', $user->id);
        $q->where('type', 'chat')->where('user_id', $user->id)->orWhere(function ($qq) use ($user, $trashedIds) {
            $qq->where('sender_id', $user->id);
            $qq->whereNotIn('id', $trashedIds);
        });


        if (!empty($args['search'])) {
            $q->where('subject', 'like', '%' . $args['search'] . '%');
            $q->orWhere('body', 'like', '%' . $args['search'] . '%');
        }


        $q->orderBy('updated_at', 'desc');

        $items = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $items;
    }
}
