<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UseLikesCountType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserLikesCountQuery extends Query
{
    const NAME = 'userLikesCount';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Колличество отзывов для фильтра'
    ];

    public function type(): Type
    {
        return GraphQL::type(UseLikesCountType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();
        return [
            'positive' => $user->likes->where('type', 'positive')->count(),
            'neutral' => $user->likes->where('type', 'neutral')->count(),
            'negative' => $user->likes->where('type', 'negative')->count(),
        ];
    }
}
