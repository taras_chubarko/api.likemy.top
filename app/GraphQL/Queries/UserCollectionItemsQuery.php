<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserCollectionItemsType;
use App\Models\Collection;
use App\Models\CollectionItem;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserCollectionItemsQuery extends Query
{
    const NAME = 'userCollectionItems';

    protected $attributes = [
        'name' => 'userCollectionItems',
        'description' => 'Фильмы в коллекции'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserCollectionItemsType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $collection = Collection::whereSlug($args['slug'])->first();
        $data['collection'] = $collection;

        $q = CollectionItem::query();
        $q->where('collection_id', $collection->id);
        $q->orderBy('created_at', 'desc');
                        $items = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        if($items){
            $items->getCollection()->transform(function ($value) {
                return $value->movie;
            });
        }
        $data['items'] = $items;
        return $data;
    }
}
