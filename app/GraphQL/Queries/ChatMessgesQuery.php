<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\ChatMessgeType;
use App\GraphQL\Types\ChatType;
use App\Models\Chat;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class ChatMessgesQuery extends Query
{
    protected $attributes = [
        'name' => 'chatMessges',
        'description' => 'Сообщения чата'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(ChatType::NAME);
    }

    public function args(): array
    {
        return [
            'chat_id' => ['type' => Type::nonNull(Type::int())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        $q = Chat::query();
        $q->where('chat_id', $args['chat_id']);
        $q->orderBy('created_at', 'desc');
        $items = $q->get();

        if($items){
            $items = $items->groupBy('dt')->transform(function ($value, $key){
                $today = Carbon::now();
                $yesterday = Carbon::yesterday();

                $date = $key;
                if ($key == $today->format('d.m.Y')) {
                    $date = 'Сегодня';
                }

                if ($key == $yesterday->format('d.m.Y')) {
                    $date = 'Вчера';
                }

                return [
                    'date' => $date,
                    'messages' => $value->reverse()->values(),
                ];

            })->values();

        }

        $items = $items->paginate($args['per_page'], $args['current_page']);

        return $items;

    }


    /* public function getData
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getData($args)
    {
        $date = Carbon::parse($args['date']);
        $today = Carbon::now();
        $yesterday = Carbon::yesterday();

        $exists = Chat::where('chat_id', $args['chat_id'])->first();

        if($exists){
            $q = Chat::query();
            $q->where('chat_id', $args['chat_id']);
            $q2 = clone $q;

            if ($date->format('d.m.Y') == $today->format('d.m.Y')) {
                $q->where('created_at', 'like', "{$date->format('Y-m-d')}%");

                $result['day'] = 'Сегодня';
                $result['prevDay'] = $today->subDay()->format('d.m.Y');
                $result['messages'] = $q->get();
            }
            else if ($date->format('d.m.Y') == $yesterday->format('d.m.Y')) {
                $q->where('created_at', 'like', "{$date->format('Y-m-d')}%");

                $result['day'] = 'Вчера';
                $result['prevDay'] = $yesterday->subDay()->format('d.m.Y');
                $result['messages'] = $q->get();
            }else{
                $q->where('created_at', 'like', "{$date->format('Y-m-d')}%");

                $result['day'] = $date->format('d.m.Y');
                $result['prevDay'] = $date->subDay()->format('d.m.Y');
                $result['messages'] = $q->get();
                //dd($result['messages']->count());

                if($result['messages']->count() == 0){
                    $mess = Chat::where('chat_id', $args['chat_id'])->first();
                    if($mess){
                        $q2->where('created_at', 'like', "{$mess->created_at->format('Y-m-d')}%");
                        $result['day'] = $mess->created_at->format('d.m.Y');
                        $result['prevDay'] = $mess->created_at->subDay()->format('d.m.Y');
                        $result['messages'] = $q2->get();
                    }
                }
            }

        }else{
            $result['day'] = null;
            $result['prevDay'] = null;
            $result['messages'] = [];
        }


        return $result;

    }
}
