<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieLikeType;
use App\Models\MovieLike;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieMyLikeQuery extends Query
{
    const NAME  = 'movieMyLike';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Мой лайк фильма'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieLikeType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_id' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return MovieLike::where('user_id', $user->id)->where('movie_id', $args['movie_id'])->first();
    }
}
