<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Inputs\MoviesPaginateInput;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MoviesPaginateQuery extends Query
{
    const  NAME = 'moviesPaginate';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Список фильмов с пагинацией.'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'data' => ['type' => Type::nonNull(GraphQL::type(MoviesPaginateInput::NAME))],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = $args['data'];

        if ($data['type'] == 'films') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPFilm');
        }

        if ($data['type'] == 'serials') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPSerial');
        }

        if ($data['type'] == 'cartoons') {
            $q = Movie::where('genre', 'LIKE', '%мультфильм%');
        }

        if ($data['g']) {
            $genre = $data['genre'];

            if($data['g'] == 'in'){
                $q->where(function ($g) use ($genre) {
                    foreach ($genre as $datum) {
                        $g->orWhere('genre', 'LIKE', '%' . $datum . '%');
                    }
                });
            }
            if($data['g'] == 'notin'){
                $q->where(function ($g) use ($genre) {
                    foreach ($genre as $datum) {
                        $g->orWhere('genre', 'NOT LIKE', '%' . $datum . '%');
                    }
                });
            }


        } else {
            if (isset($data['genre']) && $data['genre'][0] !== 'all') {
                $genre = $data['genre'];
                $q->where(function ($g) use ($genre) {
                    foreach ($genre as $datum) {
                        $g->orWhere('genre', 'LIKE', '%' . $datum . '%');
                    }
                });
            }
        }

        if (isset($data['country']) && $data['country'][0] !== 'all') {
            $country = $data['country'];
            $q->where(function ($g) use ($country) {
                foreach ($country as $datum) {
                    $g->orWhere('country', 'LIKE', '%' . $datum . '%');
                }
            });
        }

        if ($data['year']['from'] && $data['year']['to']) {
            $q->whereBetween('year', [$data['year']['from'], $data['year']['to']]);
        }


        if ($data['filter'] == 'popular') {
            $q->orderBy('ratingData->ratingGoodReview', 'desc');
        }

        if ($data['filter'] == 'novelty') {
            $q->orderBy('year', 'desc');
        }

        $movies = $q->paginate($data['per_page'], ['*'], 'page', $data['current_page']);

        return $movies;
    }
}
