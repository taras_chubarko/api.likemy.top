<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\CollectionType;
use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CollectionsInSettQuery extends Query
{
    const NAME = 'collectionsInSett';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Список коллеций для блока'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(CollectionType::NAME));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return $user->collections;
    }
}
