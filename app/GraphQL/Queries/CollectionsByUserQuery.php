<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\CollectionType;
use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CollectionsByUserQuery extends Query
{
    protected $attributes = [
        'name' => 'collectionsByUser',
        'description' => 'Подборки пользователей'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(CollectionType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();


        $q = Collection::query();
        $q->has('items');
        $q->where('type', 'public');
        $q->where('user_id', '<>',1);
        $q->withCount('items');
        $q->orderBy('items_count', 'desc');

        $items = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $items;
    }
}
