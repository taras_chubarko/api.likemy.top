<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use App\Models\MovieSearch;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MoviesFilmPlusFilmSearch extends Query
{
    protected $attributes = [
        'name' => 'moviesFilmPlusFilmSearch',
        'description' => 'Фильм + фильм поиск'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieType::NAME));
    }

    public function args(): array
    {
        return [
            'search' => ['type' => Type::nonNull(Type::string())],
            'type' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $ids = MovieSearch::where('text', 'like', '%' . $args['search'] . '%')->select('movie_id')->get();

        $items = [];

        if ($args['type'] == 'films') {
            $items = Movie::whereIn('id', $ids->pluck('movie_id'))
                ->where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPFilm')
                ->take(10)->get();
        }

        if ($args['type'] == 'serials') {
            $items = Movie::whereIn('id', $ids->pluck('movie_id'))
                ->where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPSerial')
                ->take(10)->get();
        }
        if ($args['type'] == 'cartoons') {
            $items = Movie::whereIn('id', $ids->pluck('movie_id'))
                ->where('genre', 'LIKE', '%мультфильм%')
                ->take(10)->get();
        }

        return $items;
    }
}
