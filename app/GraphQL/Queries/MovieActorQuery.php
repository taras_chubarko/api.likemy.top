<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MoviePeopleType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieActorQuery extends Query
{
    protected $attributes = [
        'name' => 'movieActor',
        'description' => 'Информация про актера'
    ];

    public function type(): Type
    {
        return GraphQL::type(MoviePeopleType::NAME);
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
       return KinoPoisk::getActor($args['id']);
    }
}
