<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MoviesFilmPlusFilm extends Query
{
    protected $attributes = [
        'name' => 'moviesFilmPlusFilm',
        'description' => 'Фильм + фильм подборка'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieType::NAME));
    }

    public function args(): array
    {
        return [
            'f1' => ['type' => Type::nonNull(Type::int())],
            'f2' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $ids1 = KinoPoisk::getSimilarsID($args['f1']);
        $f1 = Movie::whereIn('filmID', $ids1)->get();

        $ids2 = KinoPoisk::getSimilarsID($args['f2']);
        $f2 = Movie::whereIn('filmID', $ids2)->get();

        return $f1->merge($f2);
    }
}
