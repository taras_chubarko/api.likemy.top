<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieCommentType;
use App\Models\MovieComment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieCommentsQuery extends Query
{
    const NAME = 'movieComments';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Комментарии фильма'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieCommentType::NAME);
    }

    public function args(): array
    {
        return [
            'movie_id' => ['type' => Type::nonNull(Type::int())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'sort' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $q = MovieComment::query();
        $q->where('movie_id', $args['movie_id'])->whereNull('parent_id');

        if($args['sort'] == 'date'){
            $q->orderBy('created_at', 'desc');
        }

//        if($args['sort'] == 'positive'){
//            //$q->join('movie_likes', 'movie_comments.movie_id', '=', 'movie_likes.movie_id');
//            $q->join('movie_likes', function ($join) {
//                $join->on('movie_comments.movie_id', '=', 'movie_likes.movie_id')
//                    ->where('contacts.user_id', '>', 5);
//            });
//        }
//
//        if($args['sort'] == 'neutral'){
//
//        }
//
//        if($args['sort'] == 'negative'){
//
//        }

        if($args['sort'] == 'like_negative'){
            $q->withCount('negativeLikes')->orderBy('negative_likes_count', 'desc');
        }

        if($args['sort'] == 'like_positive'){
            $q->withCount('positiveLikes')->orderBy('positive_likes_count', 'desc');
        }


        $comments = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);
        return $comments;
    }
}
