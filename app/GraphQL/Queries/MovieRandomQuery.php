<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieRandomQuery extends Query
{
    protected $attributes = [
        'name' => 'movieRandom',
        'description' => 'Случайный фильм'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'type' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $q = Movie::query();

        if ($args['type'] == 'films') {
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
            $q->where('type', 'KPFilm');
        }

        if ($args['type'] == 'serials') {
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
            $q->where('type', 'KPSerial');
        }

        if ($args['type'] == 'cartoons') {
            $q->where('genre', 'LIKE', '%мультфильм%');
        }


        $movie = $q->inRandomOrder()->first();

        return $movie;
    }
}
