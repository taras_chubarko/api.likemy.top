<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UserProfileQuery extends Query
{
    const NAME = 'userProfile';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        return User::where('slug', $args['slug'])->first();
    }
}
