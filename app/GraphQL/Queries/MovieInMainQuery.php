<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\BookMark;
use App\Models\Movie;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieInMainQuery extends Query
{
    protected $attributes = [
        'name' => 'movieInMain',
        'description' => 'Фильмы в карусельку на главной'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
            'filter' => ['type' => Type::string()],
            'genre' => ['type' => Type::string()],
            'hide' => ['type' => Type::boolean()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {

        if ($args['type'] == 'films') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPFilm');
        }

        if ($args['type'] == 'serials') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPSerial');
        }

        if ($args['type'] == 'cartoons') {
            $q = Movie::where('genre', 'LIKE', '%мультфильм%');
        }


        if($args['genre'] !== 'все'){
            $q->where('genre',  'LIKE', '%'.$args['genre']  .'%');
        }


        if($args['filter'] == 'popular'){
            $q->leftJoin('movie_rating', 'movies.id', '=', 'movie_rating.movie_id')->orderBy('movie_rating.positivePercent', 'desc');
        }

        if($args['filter'] == 'recommend'){
            if($_SERVER['HTTP_U'] === 'null'){
                abort(401, 'Необходимо авторизоваться');
            }else{
                $u = User::find($_SERVER['HTTP_U']);
                $data['info'] = $u->myLikesInRecommend($args['type']);
            }
        }

        if($args['hide']){
            $b = BookMark::where('user_id', $_SERVER['HTTP_U'])->select('movie_id')->get();
            if($b->count()){
                $bA = $b->pluck('movie_id');
                $q->whereNotIn('movies.id', $bA);
            }
        }

        if($args['filter'] == 'novelty'){
            $q->orderBy('year', 'desc');
        }

        $movies = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $movies;
    }
}
