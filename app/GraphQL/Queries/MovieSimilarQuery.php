<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieSimilarQuery extends Query
{
    const NAME = 'movieSimilar';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Похожие фильми по filmId'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieType::NAME));
    }

    public function args(): array
    {
        return [
            'filmID' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $ids = KinoPoisk::getSimilarsID($args['filmID']);
        return Movie::whereIn('filmID', $ids)->get();
    }
}
