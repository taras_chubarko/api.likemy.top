<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieInMainType;
use App\Models\BookMark;
use App\Models\Movie;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MovieListQuery extends Query
{
    const NAME = 'movieList';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Список фильмов на главной в листалке'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieInMainType::NAME);
    }

    public function args(): array
    {
        return [
            'take' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
            'filter' => ['type' => Type::string()],
            'genre' => ['type' => Type::string()],
            'hide' => ['type' => Type::boolean()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        //dd($fields);

        if ($args['type'] == 'films') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPFilm');
        }

        if ($args['type'] == 'serials') {
            $q = Movie::where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPSerial');
        }

        if ($args['type'] == 'cartoons') {
            $q = Movie::where('genre', 'LIKE', '%мультфильм%');
        }


        if($args['genre'] !== 'все'){
            $q->where('genre',  'LIKE', '%'.$args['genre']  .'%');
        }


        if($args['filter'] == 'popular'){
            $q->leftJoin('movie_rating', 'movies.id', '=', 'movie_rating.movie_id')->orderBy('movie_rating.positivePercent', 'desc');
        }

        if($args['filter'] == 'recommend'){
            if($_SERVER['HTTP_U'] === 'null'){
                abort(401, 'Необходимо авторизоваться');
            }else{
                $u = User::find($_SERVER['HTTP_U']);
                $data['info'] = $u->myLikesInRecommend($args['type']);
            }
        }

        if($args['hide']){
            $b = BookMark::where('user_id', $_SERVER['HTTP_U'])->select('movie_id')->get();
            if($b->count()){
                $bA = $b->pluck('movie_id');
                $q->whereNotIn('movies.id', $bA);
            }
        }

        if($args['filter'] == 'novelty'){
            $q->orderBy('year', 'desc');
        }

        $movies = $q->take($args['take'])->get();

        //$data['tags'] = $this->tags($movies);
        $data['tags'] = $this->newTags($args['type']);
        $data['items'] = $movies;

        return $data;
    }

    /* public function tags
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tags($movies)
    {
        $tags = [];
        foreach ($movies as $movie) {
            $tags[] = explode(',', $movie->genre);
        }
        $tags = collect($tags)->collapse();

        $tagsl = collect();
        foreach ($tags as $tag) {
            $tagsl[] = trim($tag);
        }
        $tagsl = $tagsl->unique()->values();


        $tagsData[] = [
            'name' => 'все',
            'slug' => 'all',
        ];
        foreach ($tagsl as $tag) {
            $tagsData[] = [
                'name' => $tag,
                'slug' => Str::slug($tag),
            ];
        }
        return $tagsData;
    }

    /* public function newTags
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function newTags($type)
    {
        $data['films'] = [
            [
                'name' => 'все',
            ],
            [
                'name' => 'комедия',
            ],
            [
                'name' => 'фантастика',
            ],
            [
                'name' => 'боевик',
            ],
            [
                'name' => 'ужасы',
            ],
            [
                'name' => 'мелодрама',
            ],
            [
                'name' => 'мистика',
            ],
        ];

        $data['serials'] = [
            [
                'name' => 'все',
            ],
            [
                'name' => 'зарубежные',
            ],
            [
                'name' => 'русские',
            ],
        ];

        $data['cartoons'] = [
            [
                'name' => 'все',
            ],
            [
                'name' => 'зарубежные',
            ],
            [
                'name' => 'русские',
            ],
            [
                'name' => 'аниме',
            ],
        ];

        return $data[$type];
    }
}
