<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Inputs\UserDegreeInput;
use App\GraphQL\Types\MinMaxType;
use App\GraphQL\Types\UserType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserDegreeQuery extends Query
{
    protected $attributes = [
        'name' => 'userDegree',
        'description' => 'Совпадение по интересам'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(UserType::NAME);
    }

    public function args(): array
    {
        return [
            'data' => ['type' => GraphQL::type(UserDegreeInput::NAME)]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = $args['data'];

        $user = User::whereSlug($data['slug'])->first();
        $q = User::query();
        //$q->whereIn('id', $chosens);
        $q->withCount('likes');
        $q->withCount('movieComments');

        if($data['search'] && $data['search'] !== ''){
            $q->where('name', 'like', '%'.$data['search'].'%');
            $q->orWhere('surname', 'like', '%'.$data['search'].'%');
            $q->orWhere('login', 'like', '%'.$data['search'].'%');
        }

        $items = $q->get();//$q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        //if($data['percent']['min'] > 31){
            $items = $items->where('percent_degree', '>=', $data['percent']['min']);
       //}

        if($data['sortBy'] == 'desc'){
            $items = $items->sortByDesc($data['sortField']);
        }else{
            $items = $items->sortBy($data['sortField']);
        }


        $items = $items->paginate($data['per_page'], $data['current_page']);
        return $items;
    }
}
