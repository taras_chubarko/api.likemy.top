<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Inputs\UseCommentsMovieInput;
use App\GraphQL\Types\MovieCommentType;
use App\Models\MovieComment;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserCommentsMovieQuery extends Query
{
    protected $attributes = [
        'name' => 'userCommentsMovie',
        'description' => 'Комментарии фильмов пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieCommentType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'filter' => ['type' => GraphQL::type(UseCommentsMovieInput::NAME)],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();
        $data = $args['filter'];

        $q = MovieComment::query();
        $q->whereNull('parent_id');
        $q->where('user_id', $user->id);

        if($data['poisk']){
            $q->where('comment', 'LIKE', '%'.$data['poisk'].'%');
        }

        if($data['sort'] == 'date'){
            $q->orderBy('created_at', 'desc');
        }

        if($data['sort'] == 'like_negative'){
            $q->withCount('negativeLikes')->orderBy('negative_likes_count', 'desc');
        }

        if($data['sort'] == 'like_positive'){
            $q->withCount('positiveLikes')->orderBy('positive_likes_count', 'desc');
        }



        $items = $q->paginate($data['per_page'], ['*'], 'page', $data['current_page']);
        return $items;
    }
}
