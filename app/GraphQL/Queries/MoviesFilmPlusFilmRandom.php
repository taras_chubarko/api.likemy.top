<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MoviesFilmPlusFilmRandom extends Query
{
    protected $attributes = [
        'name' => 'moviesFilmPlusFilmRandom',
        'description' => 'Фильм + фильм случайный выбор'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieType::NAME));
    }

    public function args(): array
    {
        return [
            'type' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $items = [];

        if ($args['type'] == 'films') {
            $items = Movie::query()
                ->where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPFilm')
                ->inRandomOrder()
                ->take(2)->get();
        }

        if ($args['type'] == 'serials') {
            $items = Movie::query()
                ->where('genre', 'NOT LIKE', '%мультфильм%')
                ->where('genre', 'NOT LIKE', '%аниме%')
                ->where('type', 'KPSerial')
                ->inRandomOrder()
                ->take(2)->get();
        }
        if ($args['type'] == 'cartoons') {
            $items = Movie::query()
                ->where('genre', 'LIKE', '%мультфильм%')
                ->inRandomOrder()
                ->take(2)->get();
        }

        return $items;
    }
}
