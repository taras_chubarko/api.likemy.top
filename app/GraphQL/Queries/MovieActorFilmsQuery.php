<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Classes\KinoPoisk;
use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MovieActorFilmsQuery extends Query
{
    protected $attributes = [
        'name' => 'movieActorFilms',
        'description' => 'Фильмография актера'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $actor = KinoPoisk::getActor($args['id']);
        $filmography = collect($actor->filmography)->collapse()->pluck('filmID');

        $q = Movie::query();
        $q->whereIn('filmID', $filmography);
        $q->orderBy('year', 'desc');
        $movies = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        return $movies;

    }
}
