<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieSearchType;
use App\Models\Movie;
use App\Models\MovieSearch;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MovieSearchQuery extends Query
{
    protected $attributes = [
        'name' => 'movieSearch',
        'description' => 'Поиск'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(MovieSearchType::NAME));
    }

    public function args(): array
    {
        return [
            'search' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {

        $ids = MovieSearch::where('text', 'like', '%' . $args['search'] . '%')->select('movie_id')->get();


        $films = Movie::whereIn('id', $ids->pluck('movie_id'))
            ->where('genre', 'NOT LIKE', '%мультфильм%')
            ->where('genre', 'NOT LIKE', '%аниме%')
            ->where('type', 'KPFilm')
            ->take(5)->get();

        $serials = Movie::whereIn('id', $ids->pluck('movie_id'))
            ->where('genre', 'NOT LIKE', '%мультфильм%')
            ->where('genre', 'NOT LIKE', '%аниме%')
            ->where('type', 'KPSerial')
            ->take(5)->get();

        $cartoons = Movie::whereIn('id', $ids->pluck('movie_id'))
            ->where('genre', 'LIKE', '%мультфильм%')
            ->take(5)->get();


        $data = [];

        if($films->count()){
            $data[] = [
                'name' => 'Фильмы',
                'results' => $films
            ];
        }

        if($serials->count()){
            $data[] = [
                'name' => 'Сериалы',
                'results' => $serials
            ];
        }

        if($cartoons->count()){
            $data[] = [
                'name' => 'Мультфильмы',
                'results' => $cartoons
            ];
        }

        return $data;
    }
}
