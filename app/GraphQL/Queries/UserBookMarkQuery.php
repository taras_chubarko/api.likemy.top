<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\BookMark;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserBookMarkQuery extends Query
{
    const NAME = 'userBookMark';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Закладки пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        $q = BookMark::query();
        $q->where('user_id', $user->id);
        $q->where('type', $args['type']);
        $q->orderBy('created_at', 'desc');
        $movies = $q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        if($movies){
            $movies->getCollection()->transform(function ($value) {
                return $value->movie;
            });
        }

        return $movies;
    }
}
