<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MessagesUnreadQuery extends Query
{
    protected $attributes = [
        'name' => 'messagesUnread',
        'description' => 'Не прочитаные сообщения'
    ];

    public function type(): Type
    {
        return Type::int();
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return $user->unreadMessages;
    }
}
