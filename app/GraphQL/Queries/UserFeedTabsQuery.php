<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserProfileTabsType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserFeedTabsQuery extends Query
{
    protected $attributes = [
        'name' => 'userFeedTabs',
        'description' => 'Вкладки ленты'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(UserProfileTabsType::NAME));
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $data = [
            [
                'name' => 'Лента новостей',
                'path' => '/feed/'.$user->slug,
            ],
            [
                'name' => 'Избранные ('.$user->chosens->count().')',
                'path' => '/feed/'.$user->slug.'/chosen',
            ],
            [
                'name' => 'Подписки ('.$user->followings->count().')',
                'path' => '/feed/'.$user->slug.'/followings',
            ],
            [
                'name' => 'Подписчики ('.$user->followers->count().')',
                'path' => '/feed/'.$user->slug.'/followers',
            ],
            [
                'name' => 'Поиск единомышлеников',
                'path' => '/feed/'.$user->slug.'/search',
            ],
        ];
        return $data;
    }
}
