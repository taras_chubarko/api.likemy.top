<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\TimeLineType;
use App\Models\User;
use App\Models\UserTimeline;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UserFeedTimeLineQuery extends Query
{
    protected $attributes = [
        'name' => 'userFeedTimeLine',
        'description' => 'Лента новостей на подписках'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(TimeLineType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'type' => ['type' => Type::nonNull(Type::string())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $followerId = $user->followers->pluck('id');
        $chosenId = $user->chosens->pluck('chosen_id');
        $merged = $followerId->merge($chosenId);

        $q = UserTimeline::query();
        $q->where('user_id', $user->id);
        $q2 = clone $q;

//        if($args['type'] == 'all'){
//            $merged = $merged->unique();
//            $q->whereIn('author_id', $merged);
//        }

        if($args['type'] == 'chosen'){
            $merged = $chosenId->unique();
            $q->whereIn('author_id', $merged);
        }
        if($args['type'] == 'follow'){
            $merged = $followerId->unique();
            $q->whereIn('author_id', $merged);
        }

        $q->orderBy('updated_at', 'desc');
        $items = $q->get();

        if($items){

            $items = $items->groupBy('dt')->transform(function ($value, $key){

                $today = Carbon::now();
                $yesterday = Carbon::yesterday();

                $date = $key;
                if ($key == $today->format('d.m.Y')) {
                    $date = 'Сегодня';
                }

                if ($key == $yesterday->format('d.m.Y')) {
                    $date = 'Вчера';
                }

                $userGroup = $value->groupBy('author_id')->map(function ($item, $key) {
                    //dd($item->take(1));
                    $arr = $item->take(3)->transform(function ($v) {
                        return [
                            'movie' => $v->movie,
                            'like' => $v->like,
                            'comment' => $v->comment,
                        ];
                    });
                    //dd($key);
                    return [
                        'user' => User::find($key),
                        'count' => $item->count(),
                        'movies' => $arr,
                    ];
                });

                return [
                    'date' => $date,
                    'items' => $userGroup,
                ];
            })->values();
        }


        $items = $items->paginate($args['per_page'], $args['current_page']);

        return $items;
    }
}

