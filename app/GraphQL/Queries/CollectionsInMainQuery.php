<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\CollectionType;
use App\Models\Collection;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CollectionsInMainQuery extends Query
{
    protected $attributes = [
        'name' => 'collectionsInMain',
        'description' => 'Коллекции на главной'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(CollectionType::NAME));
    }

    public function args(): array
    {
        return [
            'take' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $q = Collection::query();
        $q->has('items');
        $q->where('in_main', true);
        $q->where('type', 'public');
        $q->withCount('items');
        $q->orderBy('items_count', 'desc');

        $items = $q->take($args['take'])->get();
        return $items;
    }
}
