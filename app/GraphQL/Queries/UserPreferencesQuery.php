<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UsePreferencesType;
use App\Models\Movie;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UserPreferencesQuery extends Query
{
    const NAME = 'userPreferences';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Предпочтения пользователя'
    ];

    public function type(): Type
    {
        return GraphQL::type(UsePreferencesType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'type' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $viewed = $user->viewedMovies->pluck('movie_id');

        $q = Movie::query();
        $q->whereIn('id', $viewed);

        if ($args['type'] == 'films') {
            $q->where('type', 'KPFilm');
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
        }

        if ($args['type'] == 'serials') {
            $q->where('type', 'KPSerial');
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
        }

        if ($args['type'] == 'cartoons') {
            $q->where('genre', 'LIKE', '%мультфильм%');
        }

        $q->select(['id', 'genre', 'country', 'year', 'creators']);
        $movies = $q->get();
        $genres = $movies->pluck('genre');
        $country = $movies->pluck('country');
        $year = $movies->pluck('year');
        $creators = $movies->pluck('creators');

        $data['total'] = $movies->count();
        $data['genre'] = $this->f($genres);
        $data['county'] = $this->f($country);
        $data['year'] =  $this->year($year);//$this->f($year, 10);
        $data['actors'] = $this->actors($creators);
        return $data;
    }

    /* public function f
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function f($items, $take = 4)
    {
        $arr = [];
        foreach ($items as $item){
            $arr[] = explode(',', preg_replace('/\s+/', '', $item));
        }
        $arr = collect($arr)->collapse();
        $un = $arr->unique();
        $ff = [];
        //по жанрам логинка
        //Фильм 1 - комедия, фантастика, боевик.  Фильм 2 - боевик.  Филлм 3 - комедия, боевик.  Фильм 4 - фантастика, боевик, приключение.  Фильм 5 - боевик, комедия.  И того 11 жанров   Берем комедии их 3 высчитываем 3 в процентах от 11
        foreach ($un as $item){
            $count = $arr->filter(function ($value, $key) use ($item) {
                return $value == $item;
            })->count();
            $ff[] = [
                'name' => $item,
                'count' => $count,
                'percent' => 0,
            ];
        }
        $ff = collect($ff)->sortByDesc('count')->values()->take($take);
        $ffs = $ff->pluck('count')->sum();

        $ff = $ff->map(function ($item, $key) use ($ffs) {
            $item['percent'] = (int) round(($item['count'] / $ffs) * 100);
            return $item;
        });
        return $ff;
    }

    /* public function actors
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function actors($items)
    {
        $items = $items->collapse()->collapse()->where('professionKey', 'actor');
        $un = $items->unique('id')->values();
        $ff = $un->map(function ($item, $key) use ($items) {
            $count = $items->filter(function ($value, $key) use ($item) {
                return $value['id'] == $item['id'];
            })->count();
            $item['count'] = $count;
            return $item;
        });
        $ff = $ff->sortByDesc('count')->values()->take(8);
        return $ff;
    }

    /* public function year
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function year($items)
    {
        $un = $items->unique()->values();
        $itemsCount = $items->count();

        $yy = [];
        foreach ($items as $item){
            $yy[] = [
                'name' => $item
            ];
        }
        $yy = collect($yy);

        $y1900_1950 = $yy->whereBetween('name', [1900,1950])->count();
        $y1950_1960 = $yy->whereBetween('name', [1951,1960])->count();
        $y1960_1970 = $yy->whereBetween('name', [1961,1970])->count();
        $y1970_1980 = $yy->whereBetween('name', [1971,1980])->count();
        $y1980_1990 = $yy->whereBetween('name', [1981,1990])->count();
        $y1990_2000 = $yy->whereBetween('name', [1991,2000])->count();
        $y2000_2010 = $yy->whereBetween('name', [2001,2010])->count();
        $y2010_2020 = $yy->whereBetween('name', [2011,2020])->count();


       // dd($y2000_2010);

        $data = [
            [
                'labels' => [1900,1950],
                'percent' => $itemsCount > 0 ? (int) round(($y1900_1950 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [1960],
                'percent' => $itemsCount > 0 ? (int) round(($y1950_1960 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [1970],
                'percent' => $itemsCount > 0 ? (int) round(($y1960_1970 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [1980],
                'percent' => $itemsCount > 0 ? (int) round(($y1970_1980 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [1990],
                'percent' => $itemsCount > 0 ? (int) round(($y1980_1990 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [2000],
                'percent' => $itemsCount > 0 ? (int) round(($y1990_2000 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [2010],
                'percent' => $itemsCount > 0 ? (int) round(($y2000_2010 / $itemsCount) * 100) : 0,
            ],
            [
                'labels' => [2020],
                'percent' => $itemsCount > 0 ? (int) round(($y2010_2020 / $itemsCount) * 100) : 0,
            ],
        ];

       return $data;
    }
}
