<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\UserType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UserFollowersQuery extends Query
{
    const NAME = 'userFollowers';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Подписчики'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(UserType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'search' => ['type' => Type::string()],
            'sortBy' => ['type' => Type::string()],
            'sortField' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::whereSlug($args['slug'])->first();

        $q = $user->followers();
        $q->withCount('likes');
        $q->withCount('movieComments');

        if($args['search'] && $args['search'] !== ''){
            $q->where('name', 'like', '%'.$args['search'].'%');
            $q->orWhere('surname', 'like', '%'.$args['search'].'%');
            $q->orWhere('login', 'like', '%'.$args['search'].'%');
        }

        $items = $q->get();//$q->paginate($args['per_page'], ['*'], 'page', $args['current_page']);

        if($args['sortBy'] == 'desc'){
            $items = $items->sortByDesc($args['sortField']);
        }else{
            $items = $items->sortBy($args['sortField']);
        }

        $items = $items->paginate($args['per_page'], $args['current_page']);

        return $items;
    }
}
