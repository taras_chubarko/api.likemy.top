<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MovieType;
use App\Models\Movie;
use App\Models\MovieView;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MovieTopDayQuery extends Query
{
    protected $attributes = [
        'name' => 'movieTopDay',
        'description' => 'Фильм дня'
    ];

    public function type(): Type
    {
        return GraphQL::type(MovieType::NAME);
    }

    public function args(): array
    {
        return [
            'type' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $q = Movie::query();
        $q2 = clone $q;

        if ($args['type'] == 'films') {
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
            $q->where('type', 'KPFilm');

            $q2->where('genre', 'NOT LIKE', '%мультфильм%');
            $q2->where('genre', 'NOT LIKE', '%аниме%');
            $q2->where('type', 'KPFilm');
        }

        if ($args['type'] == 'serials') {
            $q->where('genre', 'NOT LIKE', '%мультфильм%');
            $q->where('genre', 'NOT LIKE', '%аниме%');
            $q->where('type', 'KPSerial');

            $q2->where('genre', 'NOT LIKE', '%мультфильм%');
            $q2->where('genre', 'NOT LIKE', '%аниме%');
            $q2->where('type', 'KPSerial');
        }

        if ($args['type'] == 'cartoons') {
            $q->where('genre', 'LIKE', '%мультфильм%');

            $q2->where('genre', 'LIKE', '%мультфильм%');
        }

        $q->whereHas('views', function ($query){
            $today = Carbon::now();
            $query->where('updated_at', 'like', "{$today->format('Y-m-d')}%")->orderBy('view', 'desc');
        });

        $movie = $q->first();

        return $movie ? $movie : $q2->inRandomOrder()->first();
    }
}
