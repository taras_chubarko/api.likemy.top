<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\GraphQL\Types\MessageType;
use App\Models\Message;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class MessageReadQuery extends Query
{
    protected $attributes = [
        'name' => 'messageRead',
        'description' => 'Сообщение'
    ];

    public function type(): Type
    {
        return GraphQL::type(MessageType::NAME);
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())]
        ];
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool {
        $authUser = auth()->user();
        $message = Message::find($args['id']);
        return $authUser != null && ($authUser->id == $message->user_id || $authUser->id == $message->sender_id);
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        $message = Message::find($args['id']);

        if($user->id !== $message->sender_id){
            $message->read = true;
            $message->save();
        }

        return $message;
    }
}
