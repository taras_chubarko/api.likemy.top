<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class UseCommentsMovieInput extends InputType
{
    protected $inputObject = true;

    const NAME = 'UseCommentsMovieInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'sort' => ['type' => Type::string()],
            'poisk' => ['type' => Type::string()],
        ];
    }
}
