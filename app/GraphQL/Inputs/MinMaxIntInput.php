<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class MinMaxIntInput extends InputType
{
    protected $inputObject = true;

    const NAME = 'MinMaxIntInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'min' => ['type' => Type::int()],
            'max' => ['type' => Type::int()],
        ];
    }
}
