<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\InputType;

class UserDegreeInput extends InputType
{
    protected $inputObject = true;

    const NAME = 'UserDegreeInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
            'percent' => ['type' => GraphQL::type(MinMaxIntInput::NAME)],
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'search' => ['type' => Type::string()],
            'sortBy' => ['type' => Type::string()],
            'sortField' => ['type' => Type::string()],
            'fetchPolicy' => ['type' => Type::string()],
        ];
    }
}
