<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class MessageSendInput extends InputType
{
    protected $inputObject = true;

    const NAME = 'MessageSendInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'user_id' => ['type' => Type::int()],
            'message' => ['type' => Type::string()],
        ];
    }
}
