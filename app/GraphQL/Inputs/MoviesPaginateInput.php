<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\InputType;

class MoviesPaginateInput extends InputType
{

    protected $inputObject = true;

    const NAME = 'MoviesPaginateInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
            'type' => ['type' => Type::nonNull(Type::string())],
            'filter' => ['type' => Type::string()],
            'hideWithLikes' => ['type' => Type::boolean()],
            'year' => ['type' => GraphQL::type(FromToInput::NAME)],
            'rating' => ['type' => GraphQL::type(MinMaxIntInput::NAME)],
            'genre' => ['type' => Type::listOf(Type::string())],
            'country' => ['type' => Type::listOf(Type::string())],
            'g' => ['type' => Type::string()],
        ];
    }
}
