<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class FromToInput extends InputType
{

    protected $inputObject = true;

    const NAME = 'FromToInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An example input',
    ];

    public function fields(): array
    {
        return [
            'from' => ['type' => Type::string()],
            'to' => ['type' => Type::string()],
        ];
    }
}
