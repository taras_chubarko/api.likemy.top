<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 28.07.2020
 * Time: 10:55
 * https://rdoc.info/gems/kp_api/KpApi
 */

namespace App\Classes;


use App\Jobs\ParseMP4;
use App\Models\KinopoiskRating;
use App\Models\Movie;
use App\Models\MovieFilter;
use App\Models\MovieRating;
use App\Models\Parser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;

class KinoPoisk
{
    /* public function api
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function api($url)
    {
        $timestamp = time() . '' . rand(100, 600);
        $url = $url . '&uuid=' . md5(time());

        $headers = array(
            'device:android',
            'Android-Api-Version:22',
            'countryID:2',
            'ClientId:55ddecdcf6d4cd1bcaa1b3856',
            'clientDate:' . date('H:i m.d.Y'),
            'cityID:2',
            'Image-Scale:3',
            'Cache-Control:max-stale=0',
            'User-Agent:Android client (6.0.1 / api23), ru.kinopoisk/4.7.1 (1644)',
            'x-device-id:' . md5(time()),
            'X-TIMESTAMP:' . $timestamp,
            'X-SIGNATURE:' . md5($url . $timestamp . 'IDATevHDS7') . '',
        );

        $curl_options = array(
            CURLOPT_URL => 'https://ma.kinopoisk.ru/ios/5.0.0/' . $url,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $out = curl_exec($ch);
        curl_close($ch);


        return collect(json_decode($out));
    }

    /* public function parse
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function parseMovie($id)
    {
        $url = 'getKPFilmDetailView?cityID=1&still_limit=13&filmID=' . $id;
        $movie = Movie::where('filmID', $id)->first();
        if (!$movie) {
            $data = KinoPoisk::api($url);
            //dd($data);
            $item = isset($data['data']) ? $data['data'] : null;
            if ($item && !empty($item->description)) {
                //$collect = collect($item)->toArray();
                $collect = self::objectToarray($item);
                $collect['gallery'] = !empty($collect['gallery']) ? self::objectToarray($collect['gallery']) : [];

                $creators = [];
                foreach ($collect['creators'] as $creator) {
                    $creators[] = self::objectToarray($creator);
                }
                $collect['creators'] = $creators;
                //unset($collect['seriesInfo']);
                //dd($collect);
                $film = Movie::create($collect);
                //
                $p = Parser::where('filmID', $film->filmID)->first();
                $p->parsed = true;
                $p->save();
                //self::saveMP4($film);

                $rating = self::getRating($id);

                $kpRating = KinopoiskRating::where('filmID', $id)->first();
                if (!$kpRating) {
                    DB::beginTransaction();
                    KinopoiskRating::create($rating);
                    MovieRating::create([
                        'movie_id' => $film->id,
                        'negativeCount' => $rating['negativeCount'],
                        'positiveCount' => $rating['positiveCount'],
                        'neutralCount' => $rating['neutralCount'],
                        'totalCount' => $rating['totalCount'],
                        'negativePercent' => $rating['negativePercent'],
                        'positivePercent' => $rating['positivePercent'],
                        'neutralPercent' => $rating['neutralPercent'],
                        'totalPercent' => $rating['totalPercent'],
                    ]);


                    DB::commit();

                }

                self::savePoster($film);
                self::saveKadr($film);
                //event( new MovieParseEvent($film));
                $date = Carbon::now()->addMinutes(5);
                //Queue::later($date, new ParseMovie($i));
                ParseMP4::dispatch($film)->delay($date);
                return $film;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /* public function objectToarray
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function objectToarray($data)
    {
        $array = (array)$data;
        foreach ($array as $key => &$field) {
            if (is_object($field)) $field = self::objectToarray($field);
        }
        return $array;
    }

    /* public function parseFilters
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function parseFilters()
    {
        $url = 'navigatorFilters?cityID=1&still_limit=13';
        $filter = MovieFilter::first();
        if (!$filter) {
            $data = self::api($url);
            $item = isset($data['data']) ? $data['data'] : null;
            $collect = self::objectToarray($item);
            $collect['genre'] = self::objectToarray($collect['genre']);
            $collect['country'] = self::objectToarray($collect['country']);
            MovieFilter::create($collect);
            return true;
        }
        return false;
    }

    /* public function getSimilarsID
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getSimilarsID($filmID)
    {
        $url = 'getKPFilmsList?type=kp_similar_films&filmID=' . $filmID;
        $items = self::api($url);
        if (isset($items['data']->items)) {
            $collection = collect($items['data']->items);
            return $collection->pluck('id');
        } else {
            return [];
        }
    }

    /* public function getFromDirectorFilmsID
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getFromDirectorFilmsID($peopleID)
    {
        $url = 'getKPPeopleDetailView?peopleID=' . $peopleID;
        $items = self::api($url);
        if (isset($items['data']->generalFilms)) {
            $collection = collect($items['data']->generalFilms);
            return $collection->pluck('filmID');
        } else {
            return [];
        }
    }

    /* public function getRating
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getRating($filmID)
    {
        $response = Curl::to('https://kinopoisk-fvs.s3.yandex.net/films/' . $filmID . '/value-stats.json')
            ->asJson()
            ->get();
        $collection = collect($response);

        $totalCount = $collection->pluck('value')->sum();

        $negativeCount = $collection->whereIn('title', ['1', '2', '3', '4'])->pluck('value')->sum();
        $neutralCount = $collection->whereIn('title', ['5', '6'])->pluck('value')->sum();
        $positiveCount = $collection->whereIn('title', ['7', '8', '9', '10'])->pluck('value')->sum();

        $negativePercent = round(($negativeCount / $totalCount) * 100, 2);
        $positivePercent = round(($positiveCount / $totalCount) * 100, 2);
        $neutralPercent = round(($neutralCount / $totalCount) * 100, 2);
        $totalPercent = null;

        return [
            'filmID' => $filmID,
            'negativeCount' => $negativeCount,
            'positiveCount' => $positiveCount,
            'neutralCount' => $neutralCount,
            'totalCount' => $totalCount,
            'negativePercent' => $negativePercent,
            'positivePercent' => $positivePercent,
            'neutralPercent' => $neutralPercent,
            'totalPercent' => $totalPercent,
        ];
    }

    /* public function updateRating
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function updateRating()
    {
        $movies = Movie::all();
        foreach ($movies as $movie) {
            $rating = self::getRating($movie->filmID);

            $kpRating = KinopoiskRating::where('filmID', $movie->filmID)->first();
            if (!$kpRating) {
                KinopoiskRating::create($rating);
                MovieRating::create([
                    'movie_id' => $movie->id,
                    'negativeCount' => $rating['negativeCount'],
                    'positiveCount' => $rating['positiveCount'],
                    'neutralCount' => $rating['neutralCount'],
                    'totalCount' => $rating['totalCount'],
                    'negativePercent' => $rating['negativePercent'],
                    'positivePercent' => $rating['positivePercent'],
                    'neutralPercent' => $rating['neutralPercent'],
                    'totalPercent' => $rating['totalPercent'],
                ]);
            }
        }
        return true;
    }

    /* public function getActors
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getActors($filmID)
    {
        $url = 'getStaffList?filmID=' . $filmID;
        $items = self::api($url);
        if (isset($items['data']->creators)) {
            $collection = collect($items['data']->creators[1]);
            return $collection;
        } else {
            return [];
        }
    }

    /* public function getPeople
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getPeople($filmID)
    {
        $url = 'getStaffList?filmID=' . $filmID;
        $items = self::api($url);
        if (isset($items['data']->creators)) {
            $collection = collect($items['data']->creators[1]);
            return $collection;
        } else {
            return [];
        }
    }


    /* public function pp
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function pp($arr)
    {
        echo "<pre>" . print_r($arr, true) . "</pre>";
    }

    /* public function getSequelsAndPrequelsFilms
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getSequelsAndPrequelsFilms($filmID)
    {
        $url = 'getKPFilmsList?type=kp_sequels_and_prequels_films&filmID=' . $filmID;
        $items = self::api($url);
        if (isset($items['data']->items)) {
            $collection = collect($items['data']->items);
            return $collection->pluck('id');
        } else {
            return [];
        }
    }

    /* public function getActor
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getActor($peopleID)
    {
        $url = 'getKPPeopleDetailView?peopleID=' . $peopleID;
        $items = self::api($url);
        return $items['data'];
    }

    /* public function convers
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function convers()
    {
//        $config = [
//            'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
//            'ffprobe.binaries' => '/usr/bin/ffprobe',
//            'timeout'          => 3600, // The timeout for the underlying process
//            'ffmpeg.threads'   => 12,   // The number of threads that FFmpeg should use
//        ];
//
//        //$log = new Logger('FFmpeg_Streaming');
//        //$log->pushHandler(new StreamHandler('/var/log/ffmpeg-streaming.log')); // path to log file
//
//        $ffmpeg = FFMpeg::create($config);
//
//        $stream = $ffmpeg->open('https://strm.yandex.ru/vh-kp-converted/vod-content/5337746974114156640.m3u8');
//
//        $r_360p  = (new Representation)->setKiloBitrate(276)->setResize(640, 360);
//        $r_480p  = (new Representation)->setKiloBitrate(750)->setResize(854, 480);
//        $r_720p  = (new Representation)->setKiloBitrate(2048)->setResize(1280, 720);
//        $r_240p  = (new Representation)->setKiloBitrate(150)->setResize(426, 240);
//
//        $stream->dash()
//            ->setAdaption('id=0,streams=v id=1,streams=a')
//            ->x264()
//            ->addRepresentations([$r_240p, $r_360p])
//            ->save(storage_path('app/dash-stream.mpd'));
//        dd($stream);

        return self::get_m3u8_video_segment('https://strm.yandex.ru/vh-kp-converted/vod-content/5337746974114156640.m3u8');
    }

    public static function get_m3u8_video_segment($url)
    {
//        $m3u8 = file_get_contents($url);
//        //dd($m3u8);
//        if (strlen($m3u8) > 3) {
//            $tmp = strrpos($url, '/');
//            if ($tmp !== false) {
//                $base_url = substr($url, 0, $tmp + 1);
//                if ($base_url) {
//                    $array = preg_split('/\s*\R\s*/m', trim($m3u8), NULL, PREG_SPLIT_NO_EMPTY);
//                    $url2 = array();
//                    foreach ($array as $line) {
//                        $line = trim($line);
//                        if (strlen($line) > 2) {
//                            if ($line[0] != '#') {
//                                if ($line) {
//                                    $url2[] = $line;
//                                } else {
//                                    $url2[] = $base_url . $line;
//                                }
//                            }
//                        }
//                    }
//                    dd($url2);
//                    header("Content-type: application/x-mpegURL");
//                    return $url2;
//                }
//            }
//        }
//        return false;
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($ch, CURLOPT_HEADER, true);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        $lastUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
//
//        header('Content-Type: application/x-mpegurl');
//
//        readfile($lastUrl);
//
//        curl_close($ch);
        exec('ffmpeg -i ' . $url . ' -acodec copy -bsf:a aac_adtstoasc -vcodec copy ' . storage_path('app/public/video/out.mp4'));
        if (file_exists(storage_path('app/out.mp4'))) {
            return TRUE;
        } else {
            return FALSE;
        }


    }


    /* public function parseToMP4
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function parseToMP4()
    {
        $movies = Movie::all();

        foreach ($movies as $movie) {
            exec('ffmpeg -i ' . $movie->videoURL['hd'] . ' -acodec copy -bsf:a aac_adtstoasc -vcodec copy ' . storage_path('app/public/video/' . $movie->filmID . '.mp4'));
        }
    }

    /* public function saveMP4
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function saveMP4($movie)
    {
        exec('ffmpeg -y -i ' . $movie->videoHD . ' -acodec copy -bsf:a aac_adtstoasc -vcodec copy ' . storage_path('app/public/video/' . $movie->filmID . '.mp4'));
    }

    /* public function savePoster
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function savePoster($movie)
    {
        $img = \Image::make($movie->bigPosterURL);
        $img->save(storage_path('app/public/poster/' . $movie->filmID . '.jpg'));
    }

    /* public function savePoster
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function saveKadr($movie)
    {
        $img = \Image::make($movie->kadr);
        $img->save(storage_path('app/public/kadr/' . $movie->filmID . '.jpg'));
    }

    /* public function parseCron
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function parseCron()
    {
        $p = Parser::where('parsed', false)->where('parse_err', false)->first();
        Log::info('p-'.$p->filmID);
        $m = Movie::where('filmID', $p->filmID)->first();
        if(!$m){
          $film =  self::parseMovie($p->filmID);
          if(!$film){
              $p->parsed = false;
              $p->parse_err = true;
              $p->save();
          }
        }else{
            $p->parsed = true;
            $p->save();
        }
    }

}
