<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 25.09.2020
 * Time: 14:32
 */

namespace App\Classes;


use App\Models\Movie;
use App\Models\MovieSearch;

class MovieSeatchParse
{
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function index()
    {
        $movies = Movie::paginate(500);

        foreach ($movies as $movie) {
            $m = MovieSearch::where('movie_id', $movie->id)->first();
            if (!$m) {

                $txt = [];
                $txt[] = $movie->nameEN;
                $txt[] = $movie->nameRU;
                $txt[] = $movie->year;
                $txt[] = $movie->country;
                $txt[] = $movie->genre;
                //dd($movie->creators);
                foreach ($movie->creators as $creator) {
//                    dd($creator);
//                    foreach ($creator as $item){
//                        $txt[] = $item->nameEN;
//                        $txt[] = $item->nameRU;
//                    }
                    if (is_array($creator)) {
                        foreach ($creator as $item) {
                            //dd($item['nameEN']);
                            if(!empty($item['nameEN'])){
                                $txt[] = $item['nameEN'];
                            }
                            if(!empty($item['nameRU'])){
                                $txt[] = $item['nameRU'];
                            }

                        }
                    } else {
                        $txt[] = $creator->nameEN;
                        $txt[] = $creator->nameRU;
                    }

                }
                MovieSearch::create([
                    'movie_id' => $movie->id,
                    'text' => implode(', ', $txt),
                ]);

            }
        }

        return $movies;
    }
}
