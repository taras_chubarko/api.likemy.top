<?php

namespace App\Listeners;

use App\Events\MovieCommentEvent;
use App\Models\UserTimeline;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class MovieCommentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MovieCommentEvent  $event
     * @return void
     */
    public function handle(MovieCommentEvent $event)
    {
        $comment = $event->comment;

        $followers = $comment->user->followers;

        $lines = UserTimeline::where('author_id', $comment->user_id)->where('movie_id', $comment->movie_id)->get();
        //dd($lines);
        if ($lines->count()) {
            foreach ($lines as $line) {
                $line->comment_id = $comment->id;
                $line->updated_at = now();
                DB::beginTransaction();
                $line->save();
                DB::commit();
            }
        } else {
            foreach ($followers as $follower) {
                DB::beginTransaction();
                UserTimeline::create([
                    'user_id' => $follower->id,
                    'author_id' => $comment->user_id,
                    'movie_id' => $comment->movie_id,
                    'comment_id' => $comment->id,
                ]);
                DB::commit();
            }
        }
    }
}
