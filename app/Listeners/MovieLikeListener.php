<?php

namespace App\Listeners;

use App\Events\MovieLikeEvent;
use App\Models\UserTimeline;
use Illuminate\Support\Facades\DB;

class MovieLikeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MovieLikeEvent $event
     * @return void
     */
    public function handle(MovieLikeEvent $event)
    {
        //
        $like = $event->like;

        $followers = $like->user->followers;

        $lines = UserTimeline::where('author_id', $like->user_id)->where('movie_id', $like->movie_id)->get();
        //dd($lines);
        if ($lines->count()) {
            foreach ($lines as $line) {
                $line->like_id = $like->id;
                $line->updated_at = now();
                DB::beginTransaction();
                $line->save();
                DB::commit();
            }
        } else {
            foreach ($followers as $follower) {
               DB::beginTransaction();
                UserTimeline::create([
                    'user_id' => $follower->id,
                    'author_id' => $like->user_id,
                    'movie_id' => $like->movie_id,
                    'like_id' => $like->id,
                ]);
               DB::commit();
            }
        }
    }
}
