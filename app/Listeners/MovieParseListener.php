<?php

namespace App\Listeners;

use App\Classes\KinoPoisk;
use App\Events\MovieParseEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MovieParseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MovieParseEvent  $event
     * @return void
     */
    public function handle(MovieParseEvent $event)
    {
        //
        $movie = $event->movie;
        KinoPoisk::saveMP4($movie);
    }
}
