<?php

namespace App\Listeners;

use App\Events\UpdateRatingEvent;
use App\Models\Movie;
use App\Models\MovieLike;
use App\Models\MovieRating;

class UpdateRatingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateRatingEvent $event
     * @return void
     */
    public function handle(UpdateRatingEvent $event)
    {
        //
        $like = $event->like;
        $likes = MovieLike::where('movie_id', $like->movie_id)->get();

        //dd($like->movie->kinopoiskRatings);


        $totalCount = $likes->count() + $like->movie->kinopoiskRatings->totalCount;

        $negativeCount = $likes->where('type', 'negative')->count() + $like->movie->kinopoiskRatings->negativeCount;
        $positiveCount = $likes->where('type', 'positive')->count() + $like->movie->kinopoiskRatings->positiveCount;
        $neutralCount = $likes->where('type', 'neutral')->count() + $like->movie->kinopoiskRatings->neutralCount;

        $negativePercent = round(($negativeCount / $totalCount) * 100, 2);
        $positivePercent = round(($positiveCount / $totalCount) * 100, 2);
        $neutralPercent = round(($neutralCount / $totalCount) * 100, 2);
        $totalPercent = null;
        //dd($totalCount);
        $rating = MovieRating::where('movie_id', $like->movie_id)->first();
        if(!$rating){
            MovieRating::create([
                'movie_id' => $like->movie_id,
                'negativeCount' => $negativeCount,
                'positiveCount' => $positiveCount,
                'neutralCount' => $neutralCount,
                'totalCount' => $totalCount,
                'negativePercent' => $negativePercent,
                'positivePercent' => $positivePercent,
                'neutralPercent' => $neutralPercent,
                'totalPercent' => $totalPercent,
            ]);
        }else{
            $rating->update([
                'movie_id' => $like->movie_id,
                'negativeCount' => $negativeCount,
                'positiveCount' => $positiveCount,
                'neutralCount' => $neutralCount,
                'totalCount' => $totalCount,
                'negativePercent' => $negativePercent,
                'positivePercent' => $positivePercent,
                'neutralPercent' => $neutralPercent,
                'totalPercent' => $totalPercent,
            ]);
        }

    }
}
