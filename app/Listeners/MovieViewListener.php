<?php

namespace App\Listeners;

use App\Events\MovieViewEvent;
use App\Models\MovieView;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MovieViewListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MovieViewEvent  $event
     * @return void
     */
    public function handle(MovieViewEvent $event)
    {
        //
        $movie = $event->movie;
        if($movie->views){
            $movie->views()->increment('view');
        }else{
            $view = new MovieView(['view' => 1]);
            $movie->views()->save($view);
        }
    }
}
