<?php

namespace App\Console\Commands;

use App\Classes\KinoPoisk;
use Illuminate\Console\Command;

class ParseMovieCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:movie';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse movie';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        KinoPoisk::parseCron();
        return 0;
    }
}
