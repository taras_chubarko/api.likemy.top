<?php

namespace App\Http\Controllers;

use App\Classes\KinoPoisk;
use App\Classes\VideoStream;
use App\Jobs\ParseMovie;
use App\Jobs\ParseMP4;
use App\Models\Movie;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Storage;
use Streaming\FFMpeg;

class MoviesController extends Controller
{
    //
    /* public function poster
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function poster($name, $image)
    {
        $path = $name.'/'.$image;
        $cache_image = \Image::cache(function ($image) use ($path) {
            $contents ='https://st.kp.yandex.net/images/'.$path;
            $im = $image->make($contents);
            //dd($im);
            return  $im;
        }, 1440); // cache for 10 minutes
        return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
    }

    /* public function kadr
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function kadr($image)
    {
        $path = $image;
        $cache_image = \Image::cache(function ($image) use ($path) {
            $contents ='https://st.kp.yandex.net/images/kadr/'.$path;
            return $image->make($contents);
        }, 1440); // cache for 10 minutes
        return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
    }

    /* public function parseFilm
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function parseFilm($id)
    {
        $film = KinoPoisk::parseMovie($id);
        dd($film);
    }

    /* public function video
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function video($id)
    {
        //$movie = Movie::find($id);
        //videoURL
        $file = Storage::get(storage_path('app/public/video/'.$id.'.mp4'));
        return response($file, 200)->header('Content-Type', 'video/mp4');
//        $video_path = $movie->videoURL['hd'];
//        $stream = new VideoStream($video_path);
//        $stream->start();
//        $config = [
//            'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
//            'ffprobe.binaries' => '/usr/bin/ffprobe',
//            'timeout'          => 3600, // The timeout for the underlying process
//            'ffmpeg.threads'   => 12,   // The number of threads that FFmpeg should use
//        ];
//
//        $ffmpeg = FFMpeg::create($config);
//        $video = $ffmpeg->open($movie->videoURL['hd']);
//        return $video;
//        $ffmpeg = FFMpeg::create();
//        $video = $ffmpeg->open($movie->videoURL['hd']);

        //return response($video_path);
//        return response()->file(
//            file_get_contents($movie->videoURL['hd']),
//            [
//                'Content-Type' => 'application/x-mpegURL',
//                'isHls' => true
//            ]
//        );
    }

    /* public function parseFilms
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function parseFilms($from, $to)
    {
        for ($i = $from; $i < $to; $i++) {
            //dispatch( new ParseMovie($i));
            $date = Carbon::now()->addSeconds(30);
            //Queue::later($date, new ParseMovie($i));
            ParseMovie::dispatch($i)->delay($date);
            //ParseMP4::dispatch($film)->delay($date);
        }
        return 'ok';
    }
}
