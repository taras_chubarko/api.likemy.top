<?php

namespace App\Http\Controllers;

use App\Classes\KinoPoisk;
use App\Models\Movie;
use Illuminate\Http\Request;
use PhpParser\Builder\Property;

class ParseMoviesController extends Controller
{
    //
    /* public function getMovie
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getMovie($id)
    {
        return response()->json(KinoPoisk::parseMovie($id), 200);
    }
}
