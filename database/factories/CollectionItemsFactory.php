<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\CollectionItem::class, function (Faker $faker) {

    $collections = \App\Models\Collection::where('user_id', 1)->get()->pluck('id');
    $movies = \App\Models\Movie::all()->pluck('id');

    return [
        'collection_id' => $faker->randomElement($collections),
        'movie_id' => $faker->randomElement($movies),
    ];

});
