<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male','female']);

    $pathAva = storage_path().'/app/public/avatar/'.$faker->sha1.'.jpg';
    \Image::make('https://i.pravatar.cc/300')->save($pathAva);
    $imAva = pathinfo($pathAva);
    $imAva['dirname'] = 'public/avatar';

    $pathBg = storage_path().'/app/public/bg/'.$faker->sha1.'.jpg';
    \Image::make('https://placeimg.com/1600/1200/nature')->save($pathBg);
    $imBg = pathinfo($pathBg);
    $imBg['dirname'] = 'public/bg';

    return [
        'name' => $faker->firstName($gender),
        'surname' => $faker->lastName,
        'login' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'gender' => $gender,
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'avatar' => $imAva,
        'bg' => $imBg,
    ];
});
