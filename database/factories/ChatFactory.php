<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Chat::class, function (Faker $faker) {

    $sender = $faker->randomElement([64, 13]);

    return [
        'user_id' => $sender == 64 ? 13 : 64,
        'sender_id' => $sender,
        'chat_id' => 45,
        'message' => $faker->text(200),
    ];
});
