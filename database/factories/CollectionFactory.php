<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Collection::class, function (Faker $faker) {
    return [
        //
        'user_id' => 1,
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'type' => 'public',
    ];
});
