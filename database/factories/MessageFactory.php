<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Message::class, function (Faker $faker) {

    $users = \App\Models\User::all()->pluck('id');

    return [
        'user_id' => 64,
        'sender_id' => $faker->randomElement($users),
        'type' => 'chat',
        'body' => $faker->text(500),
    ];

});
