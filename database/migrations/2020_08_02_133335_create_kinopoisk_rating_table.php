<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKinopoiskRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinopoisk_rating', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('filmID');
            $table->bigInteger('negativeCount')->unsigned()->nullable();
            $table->bigInteger('positiveCount')->unsigned()->nullable();
            $table->bigInteger('neutralCount')->unsigned()->nullable();
            $table->bigInteger('totalCount')->unsigned()->nullable();
            $table->decimal('negativePercent', 10, 2)->unsigned()->nullable();
            $table->decimal('positivePercent', 10, 2)->unsigned()->nullable();
            $table->decimal('neutralPercent', 10, 2)->unsigned()->nullable();
            $table->decimal('totalPercent', 10, 2)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kinopoisk_rating');
    }
}
