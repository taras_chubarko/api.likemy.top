<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_rating', function (Blueprint $table) {
            $table->id();
            $table->foreignId('movie_id')->constrained()->onDelete('cascade');
            $table->bigInteger('negativeCount')->unsigned()->nullable();
            $table->bigInteger('positiveCount')->unsigned()->nullable();
            $table->bigInteger('neutralCount')->unsigned()->nullable();
            $table->bigInteger('totalCount')->unsigned()->nullable();
            $table->decimal('negativePercent', 10, 2)->unsigned()->nullable();
            $table->decimal('positivePercent', 10, 2)->unsigned()->nullable();
            $table->decimal('neutralPercent', 10, 2)->unsigned()->nullable();
            $table->decimal('totalPercent', 10, 2)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_rating');
    }
}
