<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id()->index();
            $table->bigInteger('filmID')->nullable()->unsigned();
            $table->string('nameRU')->nullable();
            $table->string('nameEN')->nullable();
            $table->string('type')->nullable();
            $table->string('class')->nullable();
            $table->integer('hasSimilarFilms')->nullable()->unsigned();
            $table->integer('reviewsCount')->nullable();
            $table->jsonb('ratingData')->nullable();
            $table->integer('hasSequelsAndPrequelsFilms')->nullable()->unsigned();
            $table->integer('hasRelatedFilms')->nullable()->unsigned();
            $table->string('webURL')->nullable();
            $table->string('posterURL')->nullable();
            $table->jsonb('posterSize')->nullable();
            $table->string('bigPosterURL')->nullable();
            $table->string('year')->nullable();
            $table->string('filmLength')->nullable();
            $table->string('country')->nullable();
            $table->string('genre')->nullable();
            $table->text('slogan')->nullable();
            $table->longText('description')->nullable();
            $table->jsonb('videoURL')->nullable();
            $table->string('ratingMPAA')->nullable();
            $table->string('ratingAgeLimits')->nullable();
            $table->integer('hasAwards')->nullable();
            $table->jsonb('rentData')->nullable();
            $table->jsonb('budgetData')->nullable();
            $table->jsonb('gallery')->nullable();
            $table->jsonb('creators')->nullable();
            $table->jsonb('topNewsByFilm')->nullable();
            $table->jsonb('triviaData')->nullable();
            $table->string('musicAdvert')->nullable();
            $table->integer('is3D')->nullable();
            $table->integer('hasSeance')->nullable();
            $table->integer('isIMAX')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
