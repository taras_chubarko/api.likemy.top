<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('filmID')->nullable();
            $table->boolean('parsed')->default(false);
            $table->boolean('parsed_video')->default(false);
            $table->boolean('parsed_poster')->default(false);
            $table->boolean('parsed_kadr')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser');
    }
}
