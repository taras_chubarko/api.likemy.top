<?php

use Illuminate\Database\Seeder;

class CollectionItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\Models\CollectionItem::class, 200)->create();
    }
}
