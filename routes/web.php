<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {

    //$data = \App\Models\UserSocial::first()->data;
    //dd($data);
    $url = 'getKPFilmDetailView?cityID=1&still_limit=13&filmID=1491';
    //$url = 'getStaffList?filmID=854';
    //$url = 'getKPTodayFilms';
    //$url = 'getKPReviews?filmID=854&page=2';
    //$url = 'getKPPeopleDetailView?peopleID=24546';
    //$url = 'getGallery?cityID=1&still_limit=13&filmID=854';
    //$url = 'navigatorFilters?cityID=1&still_limit=13';
    //$url = 'getKPFilmsList?type=kp_similar_films&filmID=854';
    //$url = 'getKPFilmsList?type=kp_sequels_and_prequels_films&filmID=854';
   $data = \App\Classes\KinoPoisk::api($url);
    //$data = \Illuminate\Support\Facades\Redis::connection();
    //$data = \App\Classes\KinoPoisk::getActor(8214);
    //$data = \App\Classes\KinoPoisk::parseToMP4();
   dd($data);
    //$data = \App\Classes\KinoPoisk::getFromDirectorFilmsID(24546);

    //App\Classes\KinoPoisk::parseFilters();
    //\App\Classes\KinoPoisk::parseMovie(687595);
    //$kp_data = fetch_kinopoisk_api_data('getKPTop?cityID=1&still_limit=13&type=kp_item_top_popular_films&page=1');
    //$kp_data = fetch_kinopoisk_api_data('navigatorFilters?cityID=1&still_limit=13');
    //$data = \App\Classes\KinoPoisk::parseMovie(594736);
    //$data = \App\Models\Movie::find(28);
   // $data = \App\Models\MovieComment::where('movie_id',28)->get();
    //$data = \App\Models\MovieComment::find(6);
    //dd($data[1]->type);
    //$data = \App\Models\User::find(64);
    //dd($data->viewedMovies->count());

//    $data = \App\Classes\MovieSeatchParse::index();
//    dd($data);

//    \Mail::raw('Text to e-mail', function ($message) {
//        //
//        $message->to('tchubarko@gmail.com');
//        $message->subject('test');
//    });
    //dd($_SERVER);
    //$data = \App\Classes\KinoPoisk::getRating(298);
    //dd(is_bool());

});

Route::post('parse/movie/{id}', 'ParseMoviesController@getMovie');
Route::get('poster/{name}/{image}', 'MoviesController@poster');
Route::get('kadr/{image}', 'MoviesController@kadr');
Route::get('parse/film/{id}', 'MoviesController@parseFilm');
Route::get('parse/films/{from}/{to}', 'MoviesController@parseFilms');
//Route::get('video/{id}', 'MoviesController@video');

//Route::group(['prefix' => 'social'], function () {
//
//});

