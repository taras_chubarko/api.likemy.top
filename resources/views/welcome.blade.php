<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
        <script src="https://unpkg.com/video.js/dist/video.js"></script>
        <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <img src="{{ asset('storage/avatar/1bc0e60f19ae6547c28d3af4014810f282945c28.jpg') }}" class="d-block w-100">
                <video id="my_video_1" class="video-js vjs-default-skin" controls preload="auto" width="640" height="268"
                       data-setup='{}'>
                    <source src="https://yastatic.net/yandex-video-player-iframe-api-bundles/1.0-352/index.html?mq_url=https://strm.yandex.ru/vh-kp-converted/vod-content/5337746974114156640.m3u8&amp;volume=100&amp;muted=false&amp;autoplay=true&amp;preload=true" type="application/x-mpegURL">
                    {{--<source src="https://strm.yandex.ru/vh-kp-converted/vod-content/5337746974114156640.m3u8" type="application/x-mpegURL">--}}
                    {{--<source src="https://api-likemytop.progim.net/test" type="application/x-mpegURL">--}}
                </video>

                {{--<iframe style="border:0;width:100%;height:100%;" allowfullscreen="true" src="https://yastatic.net/yandex-video-player-iframe-api-bundles/1.0-352/index.html?mq_url=https%3A%2F%2Fstrm.yandex.ru%2Fvh-kp-converted%2Fott-content%2F135313568449775983%2Fhls%2Fmaster_sdr_hd_avc_aac.m3u8&amp;volume=100&amp;muted=false&amp;autoplay=true&amp;preload=true"></iframe>--}}

                {{--<video id="yohoho" data-kinopoisk="304">--}}
                    {{--<source src="//yohoho.cc/yo.mp4" type="video/mp4">--}}
                {{--</video>--}}
                {{--<script src="//yohoho.cc/yo.js"></script>--}}


                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
